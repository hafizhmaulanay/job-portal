<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Announcement extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'announcement';

    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'slug',
        'company_id',
        'position_id',
        'start_date',
        'end_date',
        'status_recruitment',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // One to Many ------------------------//
    public function company()
    {
        return $this->belongsTo('App\Models\MasterData\Company', 'company_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\MasterData\Position', 'position_id', 'id');
    }
}
