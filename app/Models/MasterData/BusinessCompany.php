<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessCompany extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'business_company';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_experience()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserExperience', 'business_company_id');
    }
}
