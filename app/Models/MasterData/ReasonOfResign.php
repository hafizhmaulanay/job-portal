<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReasonOfResign extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'reason_of_resign';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_experience()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserExperience', 'reason_of_resign_id');
    }
}
