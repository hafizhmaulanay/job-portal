<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'country';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_education()
    {
        return $this->hasMany('App\Models\MasterData\UserEducation', 'country_id');
    }

    public function provinces()
    {
        return $this->hasMany('App\Models\MasterData\Provinces', 'country_id');
    }

    public function user_address()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserAddress', 'country_id');
    }

}
