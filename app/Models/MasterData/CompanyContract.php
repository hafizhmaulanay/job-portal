<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyContract extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'company_contract';

    protected $dates = [
        'start_period',
        'end_period',
        'payment_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'start_period',
        'end_period',
        'status',
        'payment_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function company_contract_history()
    {
        return $this->hasMany(CompanyContractHistory::class, 'company_contract_id');
    }

}
