<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
     // use HasFactory;
     use SoftDeletes;

     public $table = 'company';

     protected $dates = [
         'created_at',
         'updated_at',
         'deleted_at',
     ];

     protected $fillable = [
         'users_id',
         'pic_name',
         'contact',
         'description',
         'created_at',
         'updated_at',
         'deleted_at',
     ];

    //  one to one ----------------------- //
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }

     // one to many --- //
    public function company_contract_history()
    {
        return $this->hasMany('App\Models\MasterData\CompanyContractHistory', 'company_id');
    }
}
