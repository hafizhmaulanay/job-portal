<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Religion extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'religion';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_personal_information()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserPersonalInformation', 'religion_id');
    }
}
