<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Villages extends Model
{
    // use HasFactory;
    // use SoftDeletes;

    public $table = 'villages';

    // protected $dates = [
    //     'created_at',
    //     'updated_at',
    //     'deleted_at',
    // ];

    protected $fillable = [
        'districts_id',
        'name',
        'alt_name',
        'latitude',
        'longitude',
    ];

    // one to many --- //
    public function user_address()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserAddress', 'villages_id');
    }

    public function districts()
    {
        return $this->belongsTo('App\Models\MasterData\Districts', 'districts_id', 'id');
    }
}
