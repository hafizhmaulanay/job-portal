<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyContractHistory extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'company_contract_history';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'company_id',
        'company_contract_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
    public function company_contract()
    {
        return $this->belongsTo(CompanyContract::class, 'company_contract_id');
    }
}
