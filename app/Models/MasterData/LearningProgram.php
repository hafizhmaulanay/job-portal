<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LearningProgram extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'learning_program';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_education()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserEducation', 'learning_program_id');
    }
}
