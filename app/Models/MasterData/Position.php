<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'position';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_personal_information()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserPersonalInformation', 'position_id');
    }
}
