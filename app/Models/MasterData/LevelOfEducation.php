<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LevelOfEducation extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'level_of_education';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_education()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserEducation', 'level_of_education_id');
    }
}
