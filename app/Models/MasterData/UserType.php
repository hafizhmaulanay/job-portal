<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserType extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'user_type';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user()
    {
        return $this->hasMany('App\Models\User', 'user_type_id');
    }

    public function request_activity_history()
    {
        return $this->hasMany('App\Models\Request\RequestActivityHistory', 'user_type_id');
    }
}
