<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalaryType extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'salary_type';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user_experience_first_salary()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserExperience', 'first_salary_type_id');
    }

    public function user_experience_last_salary()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserExperience', 'last_salary_type_id');
    }
}
