<?php

namespace App\Models\ManagementAccess;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserResume extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'user_resume';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'users_id',
        'resume_name',
        'resume_file',
        'resume_file_path',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }
}
