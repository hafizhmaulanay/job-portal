<?php

namespace App\Models\ManagementAccess;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserExperience extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'user_experience';

    protected $dates = [
        'start_period',
        'end_period',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'users_id',
        'company',
        'position',
        'start_period',
        'end_period',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }
}
