<?php

namespace App\Models\ManagementAccess;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPersonalInformation extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'user_personal_information';

    protected $dates = [
        'birth_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'users_id',
        'number_id',
        'photo',
        'photo_url',
        'contact_number',
        'birth_date',
        'gender',
        'religion_id',
        'provinces_id',
        'regencies_id',
        'districts_id',
        'address',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }

    public function religion()
    {
        return $this->belongsTo('App\Models\MasterData\Religion', 'religion_id', 'id');
    }

    public function provinces()
    {
        return $this->belongsTo('App\Models\MasterData\Provinces', 'provinces_id', 'id');
    }

    public function regencies()
    {
        return $this->belongsTo('App\Models\MasterData\Regencies', 'regencies_id', 'id');
    }

    public function districts()
    {
        return $this->belongsTo('App\Models\MasterData\Districts', 'districts_id', 'id');
    }
}
