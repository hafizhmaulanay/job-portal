<?php

namespace App\Models\ManagementAccess;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'permission';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'menus_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // many to many --- //
    public function role()
    {
        return $this->belongsToMany('App\Models\ManagementAccess\Role');
    }

    // one to many --- //
    public function menu()
    {
        return $this->belongsTo('App\Models\ManagementAccess\Menu', 'menu_id', 'id');
    }
}
