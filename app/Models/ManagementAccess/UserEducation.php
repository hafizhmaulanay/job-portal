<?php

namespace App\Models\ManagementAccess;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserEducation extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'user_education';

    protected $dates = [
        'start_period',
        'end_period',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'users_id',
        'institution',
        'level_of_education_id',
        'learning_program_id',
        'start_period',
        'end_period',
        'provinces_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }

    public function level_of_education()
    {
        return $this->belongsTo('App\Models\MasterData\LevelOfEducation', 'level_of_education_id', 'id');
    }

    public function learning_program()
    {
        return $this->belongsTo('App\Models\MasterData\LearningProgram', 'learning_program_id', 'id');
    }

    public function provinces()
    {
        return $this->belongsTo('App\Models\MasterData\Provinces', 'provinces_id', 'id');
    }
}
