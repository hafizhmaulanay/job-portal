<?php

namespace App\Models\ManagementAccess;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'menu';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'platform',
        'information',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function permission()
    {
        return $this->hasMany('App\Models\ManagementAccess\Permission', 'menu_id');
    }
}
