<?php

namespace App\Models\Operational;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobVacancyWishlist extends Model
{
    // use HasFactory;
    // use SoftDeletes;

    public $table = 'job_vacancy_wishlist';
    public $timestamps = false;

    // protected $dates = [
    //     'created_at',
    //     'updated_at',
    //     'deleted_at',
    // ];

    protected $guarded = [
        'user_id',
        'job_vacancy_id',
    ];

    // one to many --- //
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function job_vacancy()
    {
        return $this->belongsTo('App\Models\Operational\JobVacancy', 'job_vacancy_id', 'id');
    }
}
