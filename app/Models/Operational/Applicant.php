<?php

namespace App\Models\Operational;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Applicant extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'applicant';

    protected $dates = [
        'schedule_test_date',
        'schedule_interview_date',
        'schedule_offer_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'users_id',
        'job_vacancy_id',
        'schedule_test_date',
        'schedule_interview_date',
        'schedule_offer_date',
        'recomendation',
        'present_test',
        'present_interview',
        'present_offer',
        'step',
        'location_test',
        'location_interview',
        'location_offer',
        'note_test',
        'note_interview',
        'note_offer',
        'note_result_test',
        'note_result_interview',
        'note_result_offer',
        'result_test',
        'result_interview',
        'result_offer',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // one to many --- //
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id');
    }

    public function job_vacancy()
    {
        return $this->belongsTo('App\Models\Operational\JobVacancy', 'job_vacancy_id');
    }
}
