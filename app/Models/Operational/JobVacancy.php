<?php

namespace App\Models\Operational;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobVacancy extends Model
{
    // use HasFactory;
    use SoftDeletes;

    public $table = 'job_vacancy';

    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'company_id',
        'position_id',
        'provinces_id',
        'level_of_education_id',
        'learning_program_id',
        'job_type_id',
        'experience_year',
        'description',
        'start_date',
        'end_date',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // many to many --- //
    public function benefit()
    {
        return $this->belongsToMany('App\Models\MasterData\Benefit');
    }

    // one to many --- //
    public function company()
    {
        return $this->belongsTo('App\Models\MasterData\Company', 'company_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\MasterData\Position', 'position_id', 'id');
    }

    public function provinces()
    {
        return $this->belongsTo('App\Models\MasterData\Provinces', 'provinces_id', 'id');
    }

    public function level_of_education()
    {
        return $this->belongsTo('App\Models\MasterData\LevelOfEducation', 'level_of_education_id', 'id');
    }

    public function learning_program()
    {
        return $this->belongsTo('App\Models\MasterData\LearningProgram', 'learning_program_id', 'id');
    }

    public function job_type()
    {
        return $this->belongsTo('App\Models\MasterData\JobType', 'job_type_id', 'id');
    }
}
