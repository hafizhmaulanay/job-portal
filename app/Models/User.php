<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;

use ProtoneMedia\LaravelVerifyNewEmail\MustVerifyNewEmail;

class User extends Authenticatable implements MustVerifyEmail
{
    use SoftDeletes;
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use MustVerifyNewEmail;
    use TwoFactorAuthenticatable;

    public $table = 'users';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'email_verified_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_type_id',
        'name',
        'email',
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
        'remember_token',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // create role --- //
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::created(function (User $user) {
            $registrationRole = config('panel.registration_default_role');

            if (!$user->role()->get()->contains($registrationRole)) {
                $user->role()->attach($registrationRole);
            }
        });
    }

    // one to one --- //
    public function user_personal_information()
    {
        return $this->hasOne('App\Models\ManagementAccess\UserPersonalInformation', 'users_id');
    }

    public function user_resume()
    {
        return $this->hasOne('App\Models\ManagementAccess\UserResume', 'users_id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\MasterData\Company', 'users_id');
    }

    // many to many --- //
    public function role()
    {
        return $this->belongsToMany('App\Models\ManagementAccess\Role');
    }
    
    public function skill()
    {
        return $this->belongsToMany('App\Models\MasterData\Skill');
    }

    public function job_wishlist()
    {
        return $this->belongsToMany('App\Models\Operational\JobVacancy', 'job_vacancy_wishlist');
    }


    // one to many --- //
    public function role_user()
    {
        return $this->hasMany('App\Models\ManagementAccess\RoleUser', 'user_id');
    }

    public function user_address()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserAddress', 'users_id');
    }

    public function user_education()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserEducation', 'users_id');
    }

    public function user_experience()
    {
        return $this->hasMany('App\Models\ManagementAccess\UserExperience', 'users_id');
    }

    public function user_type()
    {
        return $this->belongsTo('App\Models\MasterData\UserType', 'user_type_id', 'id');
    }

    public function applicant()
    {
        return $this->hasMany('App\Models\Operational\Applicant', 'users_id');
    }
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = [
    //     'profile_photo_url',
    // ];
}
