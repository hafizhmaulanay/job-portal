<?php

namespace App\Actions\Fortify;

use App\Models\Team;
use App\Models\User;

use App\Rules\PhoneNumber;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => [
                'required', 'string', 'max:255'
            ],
            'number_id' => [
                'required', 'string', 'size:16', 'unique:user_personal_information',
            ],
            'email' => [
                'required', 'string', 'email:rfc,dns', 'max:255', 'unique:users'
            ],
            'password' => $this->passwordRules(),
            'contact_number' => [
                'required', new PhoneNumber,
            ],
            'birth_date' => [
                'required', 'date_format:d/m/Y', 'before:today',
            ],
            'gender' => [
                'required', 'integer', 'in:1,2',
            ],
            'religion_id' => [
                'required', 'integer', 'exists:religion,id',
            ],
            'provinces_id'  => [
                'required', 'integer', 'exists:provinces,id',
            ],
            'regencies_id'  => [
                'required', 'integer', 'exists:regencies,id',
            ],
            'districts_id'  => [
                'required', 'integer', 'exists:districts,id',
            ],
            'address'  => [
                'required', 'string', 'max:2000',
            ],
            // 'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['required', 'accepted'] : '',
        ])->validate();

        // return DB::transaction(function () use ($input) {
        //     return tap(User::create([
        //         'name' => $input['name'],
        //         'email' => $input['email'],
        //         'password' => Hash::make($input['password']),
        //     ]), function (User $user) {
        //         $this->createTeam($user);
        //     });
        // });

        $user = User::create([
            'user_type_id' => 3,
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        // set contact number
        $input['contact_number'] = str_replace('+', '', $input['contact_number']);
        $input['contact_number'] = str_replace(' ', '', $input['contact_number']);
        $input['contact_number'] = str_replace('_', '', $input['contact_number']);

        // birth date ------------------------------
            $date = $input['birth_date'];
            $d = substr($date, 0, 2);
            $m = substr($date, 3, 2);
            $y = substr($date, 6);
            $date = $y . '-' . $m . '-' . $d;
            $input['birth_date'] = $date; //change format
        // -----------------------------------------------

        // Detail user
        $user->user_personal_information()->create([
            'number_id' => $input['number_id'],
            'contact_number' => $input['contact_number'],
            'birth_date' => $input['birth_date'],
            'gender' => $input['gender'],
            'religion_id' => $input['religion_id'],
            'provinces_id' => $input['provinces_id'],
            'regencies_id' => $input['regencies_id'],
            'districts_id' => $input['districts_id'],
            'address' => $input['address'],
        ]);

        // sync role by users select
        $user->role()->sync(5);

        return $user;
    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    // protected function createTeam(User $user)
    // {
    //     $user->ownedTeams()->save(Team::forceCreate([
    //         'user_id' => $user->id,
    //         'name' => explode(' ', $user->name, 2)[0]."'s Team",
    //         'personal_team' => true,
    //     ]));
    // }
}
