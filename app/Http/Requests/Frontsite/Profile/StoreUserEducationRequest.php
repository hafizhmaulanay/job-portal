<?php

namespace App\Http\Requests\Frontsite\Profile;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserEducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'level_of_education_id' => [
                'required', 'integer', 'exists:level_of_education,id',
            ],
            'learning_program_id' => [
                'required', 'integer', 'exists:learning_program,id',
            ],
            'institution' => [
                'required', 'string', 'max:255',
            ],
            'provinces_id' => [
                'required', 'integer', 'exists:provinces,id',
            ],
            'start_period' => [
                'required', 'date_format:d/m/Y', 'before_or_equal:end_period',
            ],
            'end_period' => [
                'required', 'date_format:d/m/Y', 'after:start_period',
            ],
        ];
    }
}
