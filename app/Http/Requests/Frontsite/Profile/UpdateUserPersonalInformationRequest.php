<?php

namespace App\Http\Requests\Frontsite\Profile;

use App\Rules\PhoneNumber;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserPersonalInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number_id' => [
                'required', 'string', 'size:16', Rule::unique('user_personal_information')->ignore($this->user()->id, 'users_id'),
            ],
            'contact_number' => [
                'required', new PhoneNumber,
            ],
            'birth_date' => [
                'required', 'date_format:d/m/Y', 'before:today',
            ],
            'gender' => [
                'required', 'integer', 'in:1,2'
            ],
            'religion_id' => [
                'required', 'integer', 'exists:religion,id'
            ],
            'provinces_id'  => [
                'required', 'integer', 'exists:provinces,id',
            ],
            'regencies_id'  => [
                'required', 'integer', 'exists:regencies,id',
            ],
            'districts_id'  => [
                'required', 'integer', 'exists:districts,id',
            ],
            'address'  => [
                'required', 'string', 'max:2000',
            ],
        ];
    }
}
