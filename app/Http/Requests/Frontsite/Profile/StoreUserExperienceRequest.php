<?php

namespace App\Http\Requests\Frontsite\Profile;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company' => [
                'required', 'string','max:255',
            ],
            'position' => [
                'required', 'string','max:255',
            ],
            'start_period' => [
                'required', 'date_format:d/m/Y', 'before_or_equal:end_period',
            ],
            'end_period' => [
                'required', 'date_format:d/m/Y', 'after:start_period',
            ],
        ];
    }
}
