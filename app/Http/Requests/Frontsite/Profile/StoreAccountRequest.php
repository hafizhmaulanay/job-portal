<?php

namespace App\Http\Requests\Frontsite\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Laravel\Fortify\Rules\Password;

class StoreAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required', 'string', 'max:255', 'unique:users',
            ],
            'email' => [
                'required', 'email:rfc,dns', 'string', 'max:255', 'unique:users',
            ],
            'password' => [
                 'required', 'string', new Password, 'confirmed'
            ]
        ];
    }
}
