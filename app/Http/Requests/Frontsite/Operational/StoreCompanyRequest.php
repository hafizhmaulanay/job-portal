<?php

namespace App\Http\Requests\Frontsite\Operational;

use App\Rules\PhoneNumber;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pic_name' => [
                'required', 'string', 'max:255',
            ],
            'contact' => [
                'required', new PhoneNumber,
            ],
        ];
    }
}
