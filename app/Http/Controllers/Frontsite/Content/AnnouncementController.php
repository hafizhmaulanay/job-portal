<?php

namespace App\Http\Controllers\Frontsite\Content;

use App\Http\Controllers\Controller;

use App\Models\Content\Announcement;
use App\Models\Operational\Applicant;
use App\Models\Operational\JobVacancy;

use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // group by announcements
        $announcement = Announcement::with(['company.user','position',])
                                    ->whereDate('start_date', '<=', date('Y-m-d H:i:s'))
                                    ->whereDate('end_date', '>=', date('Y-m-d H:i:s'))
                                    ->orderBy('created_at', 'desc')
                                    ->paginate(10);

        return view('pages.frontsite.content.announcment.index', compact('announcement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $announcement = Announcement::where('slug', $slug)->firstOrFail();
        
        $job_vacancy = JobVacancy::where('company_id', $announcement->company_id)
                                    ->where('position_id', $announcement->position_id)
                                    ->firstOrFail();
        
        $applicant = Applicant::with(['user', 'job_vacancy.position','job_vacancy.company.user'])
                                ->where('job_vacancy_id', $job_vacancy->id)
                                ->where('step', $announcement->status_recruitment)
                                ->paginate(500);
                        
        return view('pages.frontsite.content.announcment.show', compact('applicant', 'announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }
}
