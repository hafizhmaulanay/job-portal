<?php

namespace App\Http\Controllers\Frontsite\Profile;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Profile\UpdateUserResumeRequest;

use App\Models\ManagementAccess\UserResume;

use Illuminate\Http\Request;

use File;

class UserResumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserResumeRequest $request, $id)
    {
        // update tenant type
        $data = $request->all();

        if ($request->hasFile('resume')) {

            // get current domain dan set to storage link
            $data['resume_file_path'] = $request->getSchemeAndHttpHost().'/storage'.'/';

            $data['resume_file'] = $request->file('resume')->store(
                'assets/user-resume', 'public'
            );

            // get file name
            $data['resume_name'] = $request->file('resume')->getClientOriginalName();

            // first checking old photo to delete from storage
            $get_item = UserResume::where('users_id', $id)->first();

            if ($get_item) {
                $get_item = $get_item['resume_file'];
    
                $data_old = 'storage/'.$get_item;
    
                if (File::exists($data_old)) {
                    File::delete($data_old);
                }else{
                    File::delete('storage/app/public/'.$get_item);
                }
            }

            // concat current domain & path storage link file or image
            $data['resume_file_path'] = $data['resume_file_path'] . $data['resume_file'];

        }

        // create or update
        UserResume::updateOrCreate(
            ['users_id' => $id],
            $data
        );

        alert()->success('Success Message','Save data has been success');
        return redirect()->route('resume.profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_resume = UserResume::where('users_id', $id)->first();

        if ($user_resume) {

            $user_resume->forceDelete();

            $user_resume = $user_resume['resume_file'];

            $data_old = 'storage/'.$user_resume;

            if (File::exists($data_old)) {
                File::delete($data_old);
            }else{
                File::delete('storage/app/public/'.$user_resume);
            }
        }

        alert()->success('Success Message','Delete data has been success');
        return redirect()->route('resume.profile');
    }

    // custom function
    public function resume()
    {
        $user = auth()->user();

        $user->load(['user_resume']);

        return view('pages.frontsite.profile.resume.index', compact('user'));
    }
}
