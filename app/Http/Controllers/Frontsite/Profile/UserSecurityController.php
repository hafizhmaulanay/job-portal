<?php

namespace App\Http\Controllers\Frontsite\Profile;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Profile\UpdateUserSecurityEmailRequest;
use App\Http\Requests\Frontsite\Profile\UpdateUserSecurityPasswordRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserSecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }

     // custom function
    public function security()
    {
        $user = auth()->user();

        return view('pages.frontsite.profile.security.index', compact('user'));
    }

    public function update_email(UpdateUserSecurityEmailRequest $request)
    {
        $data = $request->all();

        $user = auth()->user();

         // generates a token and sends a verification mail
        if ($user->email != $data['email']) {
            $user->newEmail($data['email']);
        }

        alert()->success('Success Message', 'Update email has been success');
        return redirect()->route('security.profile');
    }

    public function update_password(UpdateUserSecurityPasswordRequest $request)
    {
        $data = $request->all();

        $user = auth()->user();

        // sets the new password
        $data['password'] = Hash::make($data['password']);

        // updates
        $user->update($data);

        alert()->success('Success Message', 'Update password has been success');
        return redirect()->route('security.profile');
    }
}
