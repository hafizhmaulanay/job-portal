<?php

namespace App\Http\Controllers\Frontsite\Profile;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Profile\AddJobWishlistRequest;

use App\Models\Operational\JobVacancyWishlist;

use Illuminate\Http\Request;

class JobWishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddJobWishlistRequest $request)
    {
        $data = $request->all();

        $user = auth()->user();

        $user->job_wishlist()->attach($data['job_vacancy_id']);

        alert()->success('Pesan Sukses', 'Lowongan kerja ditambahkan ke wishlist Anda');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // detach
        $user = auth()->user();

        $user->job_wishlist()->detach($id);

        alert()->success('Pesan Sukses', 'Lowongan kerja dihapus dari wishlist Anda');
        return redirect()->back();
    }

    // custom function
    public function wishlist()
    {
        $user = auth()->user();

        $job_wishlist = JobVacancyWishlist::with(['job_vacancy.company.user', 'job_vacancy.company.company_contract_history.company_contract', 'job_vacancy.position', 'job_vacancy.provinces'])
                                                ->latest()
                                                ->where('user_id', $user->id)
                                                ->paginate(10);

        return view('pages.frontsite.profile.job-wishlist.index', compact('job_wishlist'));
    }
}
