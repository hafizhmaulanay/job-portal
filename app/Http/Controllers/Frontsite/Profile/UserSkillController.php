<?php

namespace App\Http\Controllers\Frontsite\Profile;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Profile\StoreUserSkillRequest;

use App\Models\MasterData\Skill;

use Illuminate\Http\Request;

class UserSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserSkillRequest $request)
    {
        $skills = $request->input('skill', []);
        $user = auth()->user();

        $user->skill()->sync($skills);

        alert()->success('Success Message', 'Save data has been success');
        return redirect()->route('skill.profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }

    // custom function
    public function skill()
    {
        $user = auth()->user();

        $user->load(['skill']);

        $skill = Skill::all()->pluck('name', 'id');

        return view('pages.frontsite.profile.skill.index', compact('user', 'skill'));
    }

    // custom function
    public function reset()
    {
        $user = auth()->user();

        $user->skill()->detach();

        alert()->success('Success Message', 'Reset data has been success');
        return redirect()->route('skill.profile');
    }
}
