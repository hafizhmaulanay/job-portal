<?php

namespace App\Http\Controllers\Frontsite\Profile;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Profile\StoreUserEducationRequest;
use App\Http\Requests\Frontsite\Profile\UpdateUserEducationRequest;

use App\Models\ManagementAccess\UserEducation;
use App\Models\MasterData\LearningProgram;
use App\Models\MasterData\LevelOfEducation;
use App\Models\MasterData\Provinces;

use Illuminate\Http\Request;

class UserEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserEducationRequest $request)
    {
        $data = $request->all();
        
        $data['users_id'] = auth()->user()->id;

        // start period ------------------------------
        $date = $data['start_period'];
        $ds = substr($date, 0, 2);
        $ms = substr($date, 3, 2);
        $ys = substr($date, 6);
        $date = $ys . '-' . $ms . '-' . $ds;
        $data['start_period'] = $date; //change format
        // -----------------------------------------------

        // end period ------------------------------
        $date = $data['end_period'];
        $de = substr($date, 0, 2);
        $me = substr($date, 3, 2);
        $ye = substr($date, 6);
        $date = $ye . '-' . $me . '-' . $de;
        $data['end_period'] = $date; //change format
        // -----------------------------------------------

        // save
        UserEducation::create($data);

        alert()->success('Success Message', 'Save data has been success');
        return redirect()->route('education.profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_education = UserEducation::findOrFail($id);

        $user_education->load(['level_of_education', 'learning_program', 'provinces',]);

        return view('pages.frontsite.profile.education.show', compact('user_education'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_education = UserEducation::findOrFail($id);

        $user_education->load(['learning_program', 'level_of_education', 'provinces']);

        $province = Provinces::all()->pluck('name', 'id');
        $level_of_education = LevelOfEducation::all()->pluck('name', 'id');
        $learning_program = LearningProgram::all()->pluck('name', 'id');

        return view('pages.frontsite.profile.education.edit', compact('user_education', 'province', 'level_of_education', 'learning_program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserEducationRequest $request, $id)
    {
        $data = $request->all();
        
        $data['users_id'] = auth()->user()->id;

        // start period ------------------------------
        $date = $data['start_period'];
        $ds = substr($date, 0, 2);
        $ms = substr($date, 3, 2);
        $ys = substr($date, 6);
        $date = $ys . '-' . $ms . '-' . $ds;
        $data['start_period'] = $date; //change format
        // -----------------------------------------------

        // end period ------------------------------
        $date = $data['end_period'];
        $de = substr($date, 0, 2);
        $me = substr($date, 3, 2);
        $ye = substr($date, 6);
        $date = $ye . '-' . $me . '-' . $de;
        $data['end_period'] = $date; //change format
        // -----------------------------------------------

        // save
        $user_education = UserEducation::findOrFail($id);
        $user_education->update($data);

        alert()->success('Success Message', 'Update data has been success');
        return redirect()->route('education.profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_education = UserEducation::findOrFail($id);
        $user_education->forceDelete();

        alert()->success('Success Message', 'Delete data has been success');
        return redirect()->route('education.profile');
    }

    // custom function
    public function education()
    {
        $user = auth()->user();
        $user_education = UserEducation::with(['level_of_education', 'learning_program'])->latest()->where('users_id', $user->id)->paginate(500);

        $province = Provinces::all()->pluck('name', 'id');
        $level_of_education = LevelOfEducation::all()->pluck('name', 'id');
        $learning_program = LearningProgram::all()->pluck('name', 'id');

        return view('pages.frontsite.profile.education.index', compact('user_education', 'province', 'level_of_education', 'learning_program'));
    }
}
