<?php

namespace App\Http\Controllers\Frontsite\Profile;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Profile\UpdateAccountRequest;
use App\Http\Requests\Frontsite\Profile\UpdateUserPersonalInformationRequest;
use App\Http\Requests\Frontsite\Profile\UploadPhotoProfileRequest;

use App\Models\ManagementAccess\UserPersonalInformation;
use App\Models\MasterData\Districts;
use App\Models\MasterData\Provinces;
use App\Models\MasterData\Regencies;
use App\Models\MasterData\Religion;
use App\Models\User;

use Illuminate\Http\Request;

use File;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $province = Provinces::all()->pluck('name', 'id');
        $religion = Religion::all()->pluck('name', 'id');

        return view('pages.frontsite.profile.personal-info.index', compact('user','province', 'religion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAccountRequest $request, UpdateUserPersonalInformationRequest $request_user_personal_information_request, $id)
    {
        // data
        $data = $request->all();

        $user = User::where('id', $id)->first();
        $user->update($data);

        $data = $request_user_personal_information_request->all();

        // set number id
        $data['number_id'] = str_replace(' ', '', $data['number_id']);
        $data['number_id'] = str_replace('_', '', $data['number_id']);

        // set mobile phone
        $data['contact_number'] = str_replace('+', '', $data['contact_number']);
        $data['contact_number'] = str_replace(' ', '', $data['contact_number']);
        $data['contact_number'] = str_replace('_', '', $data['contact_number']);

        // date of birth ------------------------------
        $date = $data['birth_date'];
        $d = substr($date, 0, 2);
        $m = substr($date, 3, 2);
        $y = substr($date, 6);
        $date = $y . '-' . $m . '-' . $d;
        $data['birth_date'] = $date; //change format
        // -----------------------------------------------

        $item = UserPersonalInformation::where('users_id', $user['id'])->first();
        $item->update($data);

        alert()->success('Success Message','Update data has been success');
        return redirect()->route('profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }

    // Custom Function
    public function upload(UploadPhotoProfileRequest $request, UserPersonalInformation $user_personal_information, $id)
    {
        // get current domain dan set to storage link
        $domain = $request->getSchemeAndHttpHost().'/storage'.'/';

        // first checking old photo to delete from storage
        $get_item = UserPersonalInformation::where('users_id', $id)->first();

        $position_string = strpos($get_item['photo'], 'assets');
        $get_item = substr($get_item['photo'], $position_string);

        $data = $get_item;
        $data = 'storage/'.$get_item;
        
        if (File::exists($data)) {
            File::delete($data);
        }else{
            $get_item = str_replace("storage/","", $data);
            File::delete('storage/app/public/'.$get_item);
        }
        
        // second get all request & set new path to request field path
        $data = $request->all();
        $data['photo'] = $request->file('photo')->store(
            'assets/photo-profile', 'public'
        );

        // concat current domain & path storage link file or photo
        $data['photo_url'] = $domain . $data['photo'];
        
        // third get first record as id user and update path
        $item = UserPersonalInformation::where('users_id', $id)->first();
        $item->update($data);

        alert()->success('Success Message','Upload photo has been success');
        return redirect()->route('profile.index');
    }

    public function reset(UserPersonalInformation $user_personal_information, $id)
    {
        // first checking old photo to delete from storage
        $get_item = UserPersonalInformation::where('users_id', $id)->first();
        $ids = $get_item['id'];

        $position_string = strpos($get_item['photo'], 'assets');
        $get_item = substr($get_item['photo'], $position_string);

        $data = 'storage/'.$get_item;
        if (File::exists($data)) {
            File::delete($data);
        }else{
            File::delete('storage/app/public/'.$get_item);
        }

        // second update value to null string
        $data = UserPersonalInformation::find($ids);
        $data->photo = null;
        $data->photo_url = null;
        $data->save();

        alert()->success('Success Message','Reset photo has been success');
        return redirect()->route('profile.index');

    }

    public function select_regency_profile($id)
    {
        // checking & validation
        $id = isset($id) ? $id : 0;

        // get data
        $regency['data'] = Regencies::where('province_id', $id)->orderBy('name', 'asc')->get();

        return response()->json($regency);

    }

    public function select_district_profile($id)
    {
        // checking & validation
        $id = isset($id) ? $id : 0;

        // get data
        $district['data'] = Districts::where('regency_id', $id)->orderBy('name', 'asc')->get();

        return response()->json($district);
    }
}
