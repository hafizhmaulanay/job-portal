<?php

namespace App\Http\Controllers\Frontsite\Profile;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Profile\StoreUserExperienceRequest;
use App\Http\Requests\Frontsite\Profile\UpdateUserExperienceRequest;

use App\Models\ManagementAccess\UserExperience;

use Illuminate\Http\Request;

class UserExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserExperienceRequest $request)
    {
        $data = $request->all();
        
        $data['users_id'] = auth()->user()->id;

        // start period ------------------------------
        $date = $data['start_period'];
        $ds = substr($date, 0, 2);
        $ms = substr($date, 3, 2);
        $ys = substr($date, 6);
        $date = $ys . '-' . $ms . '-' . $ds;
        $data['start_period'] = $date; //change format
        // -----------------------------------------------

        // end period ------------------------------
        $date = $data['end_period'];
        $de = substr($date, 0, 2);
        $me = substr($date, 3, 2);
        $ye = substr($date, 6);
        $date = $ye . '-' . $me . '-' . $de;
        $data['end_period'] = $date; //change format
        // -----------------------------------------------

        // save
        UserExperience::create($data);

        alert()->success('Success Message', 'Save data has been success');
        return redirect()->route('work_experience.profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $work_experience = UserExperience::findOrFail($id);

        return view('pages.frontsite.profile.work-experience.show', compact('work_experience'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $work_experience = UserExperience::findOrFail($id);

        return view('pages.frontsite.profile.work-experience.edit', compact('work_experience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserExperienceRequest $request, $id)
    {
        $work_experience = UserExperience::findOrFail($id);

        $data = $request->all();

        // start period ------------------------------
        $date = $data['start_period'];
        $ds = substr($date, 0, 2);
        $ms = substr($date, 3, 2);
        $ys = substr($date, 6);
        $date = $ys . '-' . $ms . '-' . $ds;
        $data['start_period'] = $date; //change format
        // -----------------------------------------------

        // end period ------------------------------
        $date = $data['end_period'];
        $de = substr($date, 0, 2);
        $me = substr($date, 3, 2);
        $ye = substr($date, 6);
        $date = $ye . '-' . $me . '-' . $de;
        $data['end_period'] = $date; //change format
        // -----------------------------------------------

        // save
        $work_experience->update($data);

        alert()->success('Success Message', 'Update data has been success');
        return redirect()->route('work_experience.profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $work_experience = UserExperience::findOrFail($id);

        $work_experience->forceDelete();

        alert()->success('Success Message', 'Delete data has been success');
        return redirect()->route('work_experience.profile');
    }

    // custom function
    public function work_experience()
    {
        $user = auth()->user();

        $work_experience = UserExperience::latest()->where('users_id', $user->id)->paginate(500);

        return view('pages.frontsite.profile.work-experience.index', compact('user', 'work_experience'));
    }
}
