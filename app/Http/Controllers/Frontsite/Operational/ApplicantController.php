<?php

namespace App\Http\Controllers\Frontsite\Operational;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Operational\ApplyJobRequest;

use App\Models\Operational\Applicant;

use Illuminate\Http\Request;

class ApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplyJobRequest $request)
    {
        $data = $request->all();

        $data['users_id'] = auth()->user()->id;

        $data['step'] = 1;

        Applicant::create($data);

        alert()->success('Pesan Sukses', 'Permohonan Anda telah dikirim');
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $my_application = Applicant::with(['job_vacancy.company.user', 'job_vacancy.position'])->findOrFail($id);

        return view('pages.frontsite.operational.application.show', compact('my_application'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }

    // custom function
    public function my_application()
    {
        $user = auth()->user();
        $my_application = Applicant::with(['job_vacancy.company.user', 'job_vacancy.position'])->latest()->where('users_id', $user->id)->paginate(500);

        return view('pages.frontsite.operational.application.index', compact('my_application'));
    }

    public function filter(Request $request, Applicant $applicant)
    {
        $data = $request->all();

        $user = auth()->user();

        // query builder 
        $applicant_query = $applicant->newQuery();

        if (isset($data['step'])) {
            $applicant_query->where('step', $data['step']);
        }

        $my_application = $applicant_query->with(['job_vacancy.company.user', 'job_vacancy.position'])
                                        ->latest()
                                        ->where('users_id', $user->id)
                                        ->paginate(500);


        return view('pages.frontsite.operational.application.index', compact('my_application'));
    }
}
