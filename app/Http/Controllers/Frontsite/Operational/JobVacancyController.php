<?php

namespace App\Http\Controllers\Frontsite\OPerational;

use App\Http\Controllers\Controller;

use App\Models\MasterData\JobType;
use App\Models\MasterData\LevelOfEducation;
use App\Models\MasterData\Provinces;

use App\Models\Operational\JobVacancy;

use Illuminate\Http\Request;

class JobVacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $job_vacancy = JobVacancy::where('uuid', $uuid)->firstOrFail();

        $job_vacancy->load(['company.user', 'position', 'provinces', 'level_of_education', 'learning_program', 'job_type', 'benefit']);

        return view('pages.frontsite.operational.job-vacancy.show', compact('job_vacancy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }

    // custom function
    public function filter(Request $request, JobVacancy $job_vacancy)
    {

        $data = $request->all();

        // get value from input
        $keyword = $data['keyword'] ?? null;

        // query builder 
        $job_vacancy_query = $job_vacancy->newQuery();

        // filter job vacancy active
        $job_vacancy_query = $job_vacancy_query->where('status', 1)
                                    ->whereDate('start_date', '<=', date('Y-m-d'))
                                    ->whereDate('end_date', '>=', date('Y-m-d'))
                                    ->whereHas('company', function($query) {
                                        $query->where('status', 1);
                                    })
                                    ->whereHas('company.company_contract_history', function($query) {
                                        $query->whereHas('company_contract', function($query) {
                                            $query->where('status', 1)->whereDate('end_period', '>=', date('Y-m-d'));
                                        });
                                    });

        if (isset($keyword)) {
            $job_vacancy_query->where(function ($query) use ($keyword) {
                $query->whereHas('position', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                })->orWhereHas('company', function ($query) use ($keyword) {
                    $query->whereHas('user', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    });
                });
            });
        }

        if (isset($data['province_id'])) {
            $job_vacancy_query->whereHas('provinces', function ($query) use ($data) {
                $query->where('id', $data['province_id']);
            });
        }

        if (isset($data['job_type_id'])) {
            $job_vacancy_query->whereHas('job_type', function ($query) use ($data) {
                $query->where('id', $data['job_type_id']);
            });
        }

        if (isset($data['level_of_education_id'])) {
            $job_vacancy_query->whereHas('level_of_education', function ($query) use ($data) {
                $query->where('id', $data['level_of_education_id']);
            });
        }

        $job_vacancy = $job_vacancy_query->with(['company.user', 'position', 'provinces'])->orderBy('created_at', 'desc')->paginate(30);

        $province = Provinces::all()->pluck('name', 'id');
        $job_type = JobType::all()->pluck('name', 'id');
        $level_of_education = LevelOfEducation::all()->pluck('name', 'id');

        return view('pages.frontsite.home-page.index', compact('job_vacancy', 'province', 'job_type', 'level_of_education'));
    }
}
