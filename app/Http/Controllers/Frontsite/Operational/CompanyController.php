<?php

namespace App\Http\Controllers\Frontsite\Operational;

use App\Http\Controllers\Controller;

use App\Http\Requests\Frontsite\Operational\StoreCompanyRequest;
use App\Http\Requests\Frontsite\Profile\StoreAccountRequest;

use App\Models\MasterData\Company;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.frontsite.operational.company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAccountRequest $request, StoreCompanyRequest $request_company)
    {
        $data_user = $request->all();

        // set user type
        $data_user['user_type_id'] = 2;

        // set hash password
        $data_user['password'] = Hash::make($data_user['password']);

        // create user
        $user = User::create($data_user);

        // sync role by users select
        $user->role()->sync(4);

        $data_company = $request_company->all();

        // set user id
        $data_company['users_id'] = $user->id;

        // set mobile phone
        if ($request->filled('contact')) {
            $data_company['contact'] = str_replace('+', '', $data_company['contact']);
            $data_company['contact'] = str_replace(' ', '', $data_company['contact']);
            $data_company['contact'] = str_replace('_', '', $data_company['contact']);
        }

        Company::create($data_company);

        return redirect()->back()->with('success', 'Silahkan cek email anda untuk update informasi akun anda.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }
}
