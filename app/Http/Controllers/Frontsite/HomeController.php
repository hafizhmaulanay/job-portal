<?php

namespace App\Http\Controllers\Frontsite;

use App\Http\Controllers\Controller;
use App\Models\MasterData\CompanyContractHistory;
use App\Models\MasterData\JobType;
use App\Models\MasterData\LevelOfEducation;
use App\Models\MasterData\Provinces;
use App\Models\Operational\JobVacancy;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job_vacancy = JobVacancy::with(['company.user', 'position', 'provinces'])
                        ->whereHas('company', function($query) {
                            $query->where('status', 1);
                        })
                        ->whereHas('company.company_contract_history', function($query) {
                            $query->whereHas('company_contract', function($query) {
                                $query->where('status', 1)
                                        ->whereDate('end_period', '>=', date('Y-m-d'));
                            });
                        })
                        ->inRandomOrder()
                        ->where(function($query) {
                            $query->where('status', 1)
                                ->whereDate('start_date', '<=', date('Y-m-d'))
                                ->whereDate('end_date', '>=', date('Y-m-d'));
                            })
                        ->paginate(30);
                        
        $province = Provinces::all()->pluck('name', 'id');
        $job_type = JobType::all()->pluck('name', 'id');
        $level_of_education = LevelOfEducation::all()->pluck('name', 'id');

        return view('pages.frontsite.home-page.index', compact('job_vacancy', 'province', 'job_type', 'level_of_education'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return abort(404);
    }
}
