<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\MasterData\Districts;
use App\Models\MasterData\Regencies;

use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function regencies($id)
    {
         // checking & validation
        $id = isset($id) ? $id : 0;

        // get data
        $regency['data'] = Regencies::where('province_id', $id)->orderBy('name', 'asc')->get();

        return response()->json($regency);
    }

    public function districts($id)
    {
         // checking & validation
        $id = isset($id) ? $id : 0;

        // get data
        $district['data'] = Districts::where('regency_id', $id)->orderBy('name', 'asc')->get();

        return response()->json($district);
    }
}
