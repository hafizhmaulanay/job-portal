<?php

use App\Http\Controllers\Api\AreaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// custom API ------------------------------- //
    // select regencies from provinces
    Route::get('regencies/{id}',  [AreaController::class,'regencies'])->name('api.select.regencies');
    // select districts from regencies
    Route::get('districts/{id}',  [AreaController::class,'districts'])->name('api.select.districts');
// end custom API ------------------------------- //
