<?php

use App\Http\Controllers\Frontsite\HomeController;

use App\Http\Controllers\Frontsite\Profile\JobWishlistController;
use App\Http\Controllers\Frontsite\Profile\ProfilesController;
use App\Http\Controllers\Frontsite\Profile\UserExperienceController;
use App\Http\Controllers\Frontsite\Profile\UserEducationController;
use App\Http\Controllers\Frontsite\Profile\UserSkillController;
use App\Http\Controllers\Frontsite\Profile\UserResumeController;
use App\Http\Controllers\Frontsite\Profile\UserSecurityController;

use App\Http\Controllers\Frontsite\Operational\ApplicantController;
use App\Http\Controllers\Frontsite\Operational\CompanyController;
use App\Http\Controllers\Frontsite\Operational\JobVacancyController;

use App\Http\Controllers\Frontsite\Content\AnnouncementController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// frontsite ------------------------------- //

    // home ------------------------------- //
    Route::get('/', [HomeController::class, 'index'])->name('home');
    // end home ------------------------------- //
    
    
    // operational ------------------------------- //
        // job vacancy
        Route::get('job_vacancy/filter', [JobVacancyController::class, 'filter'])->name('job-vacancy.filter');
        Route::resource('job_vacancy', JobVacancyController::class);
        // company
        Route::resource('company', CompanyController::class);
    // end operational ------------------------------- //


    // content ------------------------------- //
        // announcement
        Route::resource('announcement', AnnouncementController::class);
    // end content ------------------------------- //
    

Route::group(['middleware' => ['auth:sanctum']], function () {
    
    // profile ------------------------------- //
        // personal info
        Route::get('select/regency/profile/{id}', [ProfilesController::class, 'select_regency_profile'])->name('select_regency.profile');
        Route::get('select/district/profile/{id}', [ProfilesController::class, 'select_district_profile'])->name('select_district.profile');
        Route::put('upload/photo/profile/{id}', [ProfilesController::class, 'upload'])->name('upload.photo.profile');
        Route::get('reset/photo/profile/{id}', [ProfilesController::class, 'reset'])->name('reset.photo.profile');
        Route::resource('profile', ProfilesController::class)->except('show');
        // Security
        Route::get('security/profile', [UserSecurityController::class, 'security'])->name('security.profile');
        Route::put('update/email/security/profile', [UserSecurityController::class, 'update_email'])->name('update.email.security.profile');
        Route::put('update/password/security/profile', [UserSecurityController::class, 'update_password'])->name('update.password.security.profile');
        Route::resource('security', UserSecurityController::class);
        // work experience
        Route::get('work_experience/profile', [UserExperienceController::class, 'work_experience'])->name('work_experience.profile');
        Route::resource('work_experience', UserExperienceController::class);
        // education
        Route::get('education/profile', [UserEducationController::class, 'education'])->name('education.profile');
        Route::resource('education', UserEducationController::class);
        // skill
        Route::get('reset/skill/profile', [UserSkillController::class, 'reset'])->name('reset.skill.profile');
        Route::get('skill/profile', [UserSkillController::class, 'skill'])->name('skill.profile');
        Route::resource('skill', UserSkillController::class);
        // resume
        Route::get('resume/profile', [UserResumeController::class, 'resume'])->name('resume.profile');
        Route::resource('resume', UserResumeController::class);
        // job wishlist
        Route::get('job_wishlist/profile', [JobWishlistController::class, 'wishlist'])->name('job_wishlist.profile');
        Route::resource('job_wishlist', JobWishlistController::class);
    // end profile ------------------------------- //


    // operational ------------------------------- //
        // my application
        Route::get('my_application/filter', [ApplicantController::class, 'filter'])->name('applicant.filter');
        Route::get('my_application', [ApplicantController::class, 'my_application'])->name('applicant.my_application');
        // apply job
        Route::resource('applicant', ApplicantController::class)->middleware('verified');
    // end operational ------------------------------- //

});
    
// end frontsite ------------------------------- //
