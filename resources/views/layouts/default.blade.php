<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        @include('includes.frontsite.meta')

        <title>@yield('title', 'Penara Job Portal')</title>

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="{{ asset('/back-design/clients/img/apple-touch-icon.png') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/back-design/clients/img/favicon.ico') }}">

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">

        @stack('before-style')
        <!-- style -->
        @include('includes.frontsite.style')

        @stack('after-style')

    </head>
    <body>

        @include('sweetalert::alert')
        @include('includes.frontsite.header')
            @yield('content')
        @include('includes.frontsite.footer')
        @include('includes.frontsite.go-to')
        @include('includes.frontsite.toats-verify')

        @stack('before-script')

        <!-- script -->
        @include('includes.frontsite.script')

        @stack('after-script')

    </body>
</html>
