<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.auth.meta')

        <title>@yield('title') | Job Portal</title>

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="{{ asset('/back-design/clients/img/apple-touch-icon.png') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/back-design/clients/img/favicon.ico') }}">

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">

        @stack('before-style')
        <!-- style -->
        @include('includes.auth.style')

        @stack('after-style')

    </head>
    <body>

        @yield('content')

        @stack('before-script')

        <!-- script -->
        @include('includes.auth.script')

        @stack('after-script')

    </body>
</html>
