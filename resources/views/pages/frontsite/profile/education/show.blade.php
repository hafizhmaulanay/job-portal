<!-- Table -->
<table class="table table-vertical-border-striped">
    <tbody>
        <tr>
            <th>Institusi :</th>
            <td>{{ isset($user_education->institution) ? $user_education->institution : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Kualifikasi Pendidikan :</th>
            <td>{{ isset($user_education->level_of_education->name) ? $user_education->level_of_education->name : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Bidang Studi :</th>
            <td>{{ isset($user_education->learning_program->name) ? $user_education->learning_program->name : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Lokasi :</th>
            <td>{{ isset($user_education->provinces->name) ? $user_education->provinces->name : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Tanggal :</th>
            <td>{{ isset($user_education->start_period) ? $user_education->start_period->format('d, F Y') : 'N/A' }} - {{ isset($user_education->end_period) ? $user_education->end_period->format('d, F Y') : 'N/A' }}</td>
        </tr>
    </tbody>
</table>
<!-- End Table -->