@extends('layouts.default')

{{-- set section --}}
@section('title', 'Edit Pendidikan - Profile | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Edit Pendidikan</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Akun</li>
                                <li class="breadcrumb-item">Pendidikan</li>
                                <li class="breadcrumb-item active" aria-current="page">Edit</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Back to home</a>
                        </div>

                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-default">
                                <i class="bi-list"></i>
                            </span>
                            <span class="navbar-toggler-toggled">
                                <i class="bi-x"></i>
                            </span>
                        </button>
                        <!-- End Responsive Toggle Button -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">
            <div class="row">
                <div class="col-lg-3">
                    
                    @include('includes.profile.menu')

                </div>
                <!-- End Col -->

                <div class="col-lg-9">
                    <!-- Card -->
                    <div class="card">
                        <div class="card-header border-bottom">
                             <div class="row align-items-center">
                                <div class="col-sm mb-2 mb-sm-0">
                                    <h5 class="card-header-title mb-2 lh-base">Edit Pendidikan</h5>
                                </div>
                            </div>
                            <!-- End Row -->

                            <!-- Error -->
                            @if ($errors->any())
                                <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                                    <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                                    <ul class="mt-3 mb-0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <!-- End Error -->
                        </div>

                        <!-- Body -->
                        <div class="card-body">

                             <!-- Form -->
                            <form action="{{ route('education.update', $user_education->id) }}" method="POST">

                                @csrf
                                @method('PUT')

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="institution" class="col-sm-3 col-form-label form-label">Institusi</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="institution" id="institution" placeholder="Institusi" aria-label="Institusi" value="{{ old('institution', isset($user_education) ? $user_education->institution : '') }}" autocomplete="off" required>

                                        @if($errors->has('institution'))
                                            <span style="font-style: bold; color: red;">{{ $errors->first('institution') }}</span>
                                        @endif

                                    </div>
                                </div>
                                <!-- End Form -->

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="level_of_education_id" class="col-sm-3 col-form-label form-label">Kualifikasi Pendidikan</label>

                                    <div class="col-sm-9">
                                        <!-- Select -->
                                        <div class="tom-select-custom">
                                            <select class="js-select form-select" name="level_of_education_id" id="level_of_education_id"
                                                data-hs-tom-select-options='{
                                                    "placeholder": "Pilih Kualifikasi Pendidikan..."
                                                }' required>
                                                    <option value="">Pilih Kualifikasi Pendidikan...</option>
                                                    @forelse ($level_of_education as $id => $level_of_education_name)
                                                        <option value="{{ $id }}" {{ old('level_of_education_id', isset($user_education) ? $user_education->level_of_education_id : '') == $id ? 'selected' : '' }}>
                                                            {{ $level_of_education_name }}
                                                        </option>
                                                    @empty
                                                        {{-- not found --}}
                                                    @endforelse
                                            </select>

                                            @if($errors->has('level_of_education_id'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('level_of_education_id') }}</span>
                                            @endif
                                        </div>
                                        <!-- End Select -->
                                    </div>
                                </div>
                                <!-- End Form -->

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="learning_program_id" class="col-sm-3 col-form-label form-label">Bidang Studi</label>

                                    <div class="col-sm-9">
                                        <!-- Select -->
                                        <div class="tom-select-custom">
                                            <select class="js-select form-select" name="learning_program_id" id="learning_program_id"
                                                data-hs-tom-select-options='{
                                                    "placeholder": "Pilih Bidang Studi..."
                                                }' required>
                                                    <option value="">Pilih Bidang Studi...</option>
                                                    @forelse ($learning_program as $id => $learning_program_name)
                                                        <option value="{{ $id }}" {{ old('learning_program_id', isset($user_education) ? $user_education->learning_program_id : '') == $id ? 'selected' : '' }}>
                                                            {{ $learning_program_name }}
                                                        </option>
                                                    @empty
                                                        {{-- not found --}}
                                                    @endforelse
                                            </select>

                                            @if($errors->has('learning_program_id'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('learning_program_id') }}</span>
                                            @endif
                                        </div>
                                        <!-- End Select -->
                                    </div>
                                </div>
                                <!-- End Form -->

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="provinces_id" class="col-sm-3 col-form-label form-label">Lokasi</label>

                                    <div class="col-sm-9">
                                        <!-- Select -->
                                        <div class="tom-select-custom">
                                            <select class="js-select form-select" name="provinces_id" id="provinces_id"
                                                data-hs-tom-select-options='{
                                                    "placeholder": "Pilih Lokasi..."
                                                }' required>
                                                    <option value="">Pilih Lokasi...</option>
                                                    @forelse ($province as $id => $province_name)
                                                        <option value="{{ $id }}" {{ old('provinces_id', isset($user_education) ? $user_education->provinces_id : '') == $id ? 'selected' : '' }}>
                                                            {{ $province_name }}
                                                        </option>
                                                    @empty
                                                        {{-- not found --}}
                                                    @endforelse
                                            </select>

                                            @if($errors->has('provinces_id'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('provinces_id') }}</span>
                                            @endif
                                        </div>
                                        <!-- End Select -->
                                    </div>
                                </div>
                                <!-- End Form -->

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="start_period" class="col-sm-3 col-form-label form-label">Tanggal Mulai</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-lg" name="start_period" id="start_period" value="{{ old('start_period', isset($user_education->start_period) ? $user_education->start_period->format('d/m/Y') : '') }}" autocomplete="off" placeholder="Tanggal Mulai" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>

                                        @if($errors->has('start_period'))
                                            <span style="font-style: bold; color: red;">{{ $errors->first('start_period') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- End Form -->

                                 <!-- Form -->
                                <div class="row mb-4">
                                    <label for="end_period" class="col-sm-3 col-form-label form-label">Tanggal Berakhir</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-lg" name="end_period" id="end_period" value="{{ old('end_period', isset($user_education->end_period) ? $user_education->end_period->format('d/m/Y') : '') }}" autocomplete="off" placeholder="Tanggal Berakhir" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>

                                        @if($errors->has('end_period'))
                                            <span style="font-style: bold; color: red;">{{ $errors->first('end_period') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- End Form -->

                                
                                <div class="d-flex justify-content-end gap-3 mt-3">
                                    <a class="btn btn-white" href="{{ route('education.profile') }}" onclick="return confirm('Anda yakin ingin menutup halaman ini? , Setiap perubahan yang Anda buat tidak akan disimpan.')">
                                        Batal
                                    </a>
                                    <button type="submit" class="btn btn-gold" onclick="return confirm('Apakah Anda yakin ingin menyimpan data ini?')">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                            <!-- End Form -->
                    
                        </div>
                        <!-- End Body -->
                    </div>
                    <!-- End Card -->
                
                </div>
                <!-- End Col -->

            </div>
        </div>
    </main>
            
@endsection

@push('after-style')
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush


@push('after-script')
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

   <script>
        $('#start_period').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $('#end_period').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    </script> 

@endpush