@extends('layouts.default')

{{-- set section --}}
@section('title', 'Personal Info - Profile | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Personal info</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Akun</li>
                                <li class="breadcrumb-item active" aria-current="page">Personal Info</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Back to home</a>
                        </div>

                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-default">
                                <i class="bi-list"></i>
                            </span>
                            <span class="navbar-toggler-toggled">
                                <i class="bi-x"></i>
                            </span>
                        </button>
                        <!-- End Responsive Toggle Button -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">
            <div class="row">
                <div class="col-lg-3">
                   
                    @include('includes.profile.menu')

                </div>
                <!-- End Col -->

                <div class="col-lg-9">
                    <div class="d-grid gap-3 gap-lg-5">
                        <!-- Card -->
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h4 class="card-header-title">Profil</h4>

                                <!-- Error -->
                                @if ($errors->any())
                                    <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                                        <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                                        <ul class="mt-3 mb-0">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                @endif
                                <!-- End Error -->
                            </div>


                            <!-- Body -->
                            <div class="card-body">
                                
                                <!-- Form -->
                                <div class="row mb-4">
                                    <label class="col-sm-3 col-form-label form-label d-none d-sm-block">Profile photo</label>

                                    <div class="col-sm-9">
                                        <!-- Media -->
                                        <div class="d-flex align-items-center justify-content-center justify-content-sm-start flex-wrap">
                                            <!-- Avatar -->
                                            <label class="avatar avatar-xl avatar-circle" for="avatarUploader">
                                                <img id="avatarImg" src=" 
                                                        @if (auth()->user()->user_personal_information()->first()->photo_url != "" || auth()->user()->user_personal_information()->first()->photo_url != null) 
                                                            @if(File::exists('storage/'.substr(auth()->user()->user_personal_information()->first()->photo_url, strpos(auth()->user()->user_personal_information()->first()->photo_url, 'assets/'))))
                                                                
                                                                {{ auth()->user()->user_personal_information()->first()->photo_url }}
                                                            
                                                            @elseif(File::exists(str_replace(substr(auth()->user()->user_personal_information()->first()->photo_url, 0, strpos(auth()->user()->user_personal_information()->first()->photo_url, 'assets/')), 'storage/app/public/', auth()->user()->user_personal_information()->first()->photo_url)))
                                                                {{ url(str_replace(substr(auth()->user()->user_personal_information()->first()->photo_url, 0, strpos(auth()->user()->user_personal_information()->first()->photo_url, 'assets/')), 'storage/app/public/', auth()->user()->user_personal_information()->first()->photo_url)) }}
                                                            @else
                                                                {{ asset('front-design/assets/img/160x160/img1.jpg') }}
                                                            @endif
                                                        @else 
                                                            {{ asset('front-design/assets/img/160x160/img1.jpg') }} 
                                                        @endif " 
                                                        alt="users avatar" class="avatar-img">
                                            </label>

                                            <div class="d-grid d-sm-flex gap-2 ms-sm-3 ms-lg-4 mt-4 mt-sm-0">

                                                <form action="{{ route('upload.photo.profile', Auth::user()->id) }}" method="POST" enctype="multipart/form-data">

                                                    @method('PUT')
                                                    @csrf

                                                    <div class="form-attachment-btn btn btn-gold btn-sm mb-2 mb-sm-0 w-100 w-md-auto">Pilih photo
                                                        <input type="file" name="photo" class="js-file-attach form-attachment-btn-label" id="avatarUploader"
                                                            data-hs-file-attach-options='{
                                                                "textTarget": "#avatarImg",
                                                                "mode": "image",
                                                                "targetAttr": "src",
                                                                "allowTypes": [".png", ".jpeg", ".jpg"],
                                                                "maxFileSize": "2048"
                                                            }'>
                                                        </div>
                                                    <button type="submit" class="btn btn-sm btn-outline-gold w-100 w-md-auto" onclick="return confirm('Apakah Anda yakin ingin memperbarui foto ini ?')">Upload Photo</button>
                                                    <!-- End Avatar -->
                                                </form>

                                                @if (auth()->user()->user_personal_information()->first()->photo_url != "" || auth()->user()->user_personal_information()->first()->photo_url != null)
                                                    <a href="{{ route('reset.photo.profile', Auth::user()->id) }}" class="js-file-attach-reset-img btn btn-white btn-sm" onclick="return confirm('Apakah Anda yakin ingin mereset foto ini ?')">Reset</a>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- End Media -->
                                    </div>
                                </div>
                                <!-- End Form -->

                                <form action="{{ route('profile.update', Auth::user()->id) }}" method="POST">

                                    @csrf
                                    @method('PUT')

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="name" class="col-sm-3 col-form-label form-label">Nama Lengkap</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Nama Lengkap" aria-label="Nama Lengkap" value="{{ old('name') ?? $user->name  }}" autocomplete="off" required>

                                            @if($errors->has('name'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('name') }}</span>
                                            @endif

                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="number_id" class="col-sm-3 col-form-label form-label">NIK</label>

                                        <div class="col-sm-9">
                                            <input type="number" class="js-input-mask form-control" name="number_id" id="number_id" placeholder="NIK (Nomor Induk Kependudukan)"  aria-label="NIK (Nomor Induk Kependudukan)" size="16" value="{{ old('number_id') ?? $user->user_personal_information->number_id }}" autocomplete="off"  data-hs-mask-options='{
                                                "mask": "0000000000000000"
                                            }' required>

                                            @if($errors->has('number_id'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('number_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="email" class="col-sm-3 col-form-label form-label">Email</label>

                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="email" placeholder="site@gmail.com" aria-label="site@gmail.com" value="{{ old('email') ?? $user->email }}" readonly>
                                            <small class="form-text">Anda dapat mengubah alamat email melalui menu Keamanan.</small>

                                            @if($errors->has('email'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="contact_number" class="col-sm-3 col-form-label form-label">Telepon</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="js-input-mask form-control" name="contact_number" id="contact_number" placeholder="+62 xxx xxxx xxxx" value="{{ old('contact_number') ?? $user->user_personal_information->contact_number  }}" autocomplete="off" 
                                            data-hs-mask-options='{
                                                "mask": "+62 000 0000 0000"
                                            }' required>

                                            @if($errors->has('contact_number'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('contact_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="birth_date" class="col-sm-3 col-form-label form-label">Tanggal Lahir</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control form-control-lg" name="birth_date" value="{{ old('birth_date') ?? (isset($user->user_personal_information->birth_date) ? $user->user_personal_information->birth_date->format('d/m/Y') : '') }}" id="birth_date" autocomplete="off" placeholder="Tanggal Lahir" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>

                                            @if($errors->has('birth_date'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('birth_date') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="gender" class="col-sm-3 col-form-label form-label">Jenis Kelamin</label>
                                        <div class="col-sm-9">
                                            <!-- Select -->
                                            <div class="tom-select-custom">
                                                <select class="js-select form-select" name="gender" id="gender" autocomplete="off"
                                                        data-hs-tom-select-options='{
                                                            "searchInDropdown": false,
                                                            "hidePlaceholderOnSearch": true,
                                                            "placeholder": "Pilih Jenis Kelamin..."
                                                        }' required>
                                                    <option value="">Pilih Jenis Kelamin</option>
                                                    <option value="1" {{ old('gender', isset($user) ? $user->user_personal_information->gender : '')  == 1 ? 'selected' : '' }}>Pria</option>
                                                    <option value="2" {{ old('gender', isset($user) ? $user->user_personal_information->gender : '')  == 2 ? 'selected' : '' }}>Wanita</option>
                                                </select>

                                                @if($errors->has('gender'))
                                                    <span style="font-style: bold; color: red;">{{ $errors->first('gender') }}</span>
                                                @endif
                                            </div>
                                            <!-- End Select -->
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="religion" class="col-sm-3 col-form-label form-label">Agama</label>
                                        <div class="col-sm-9">
                                            <!-- Select -->
                                            <div class="tom-select-custom">
                                                <select class="js-select form-select" name="religion_id" id="religion_id" autocomplete="off"
                                                        data-hs-tom-select-options='{
                                                            "searchInDropdown": false,
                                                            "hidePlaceholderOnSearch": true,
                                                            "placeholder": "Pilih Agama..."
                                                        }' required>
                                                    <option value="">Pilih Agama</option>
                                                    @forelse($religion as $id => $religion_item_name)
                                                        <option value="{{ $id }}" {{ old('religion_id', isset($user) ? $user->user_personal_information->religion_id : '') == $id ? 'selected' : '' }}>{{ $religion_item_name }}</option>
                                                    @empty
                                                        {{-- Not found --}}
                                                    @endforelse
                                                </select>

                                                @if($errors->has('religion_id'))
                                                    <span style="font-style: bold; color: red;">{{ $errors->first('religion_id') }}</span>
                                                @endif
                                            </div>
                                            <!-- End Select -->
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="provinces_id" class="col-sm-3 col-form-label form-label">Provinsi</label>
                                        <div class="col-sm-9">
                                            <!-- Select -->
                                            <div class="tom-select-custom">

                                                <select class="js-select form-select" name="provinces_id" id="provinces_id" autocomplete="off"
                                                        data-hs-tom-select-options='{
                                                            "searchInDropdown": false,
                                                            "hidePlaceholderOnSearch": true,
                                                            "placeholder": "Pilih Provinsi..."
                                                        }' required>
                                                    <option value="">Pilih Provinsi</option>
                                                    @forelse($province as $id => $province_item_name)
                                                        <option value="{{ $id }}" {{ (isset($user) ? $user->user_personal_information->provinces_id : '') == $id ? 'selected' : '' }}>{{ $province_item_name }}</option>
                                                    @empty
                                                        {{-- Not found --}}
                                                    @endforelse
                                                </select>

                                                @if($errors->has('provinces_id'))
                                                    <span style="font-style: bold; color: red;">{{ $errors->first('provinces_id') }}</span>
                                                @endif
                                            </div>
                                            <!-- End Select -->
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="regencies_id" class="col-sm-3 col-form-label form-label">Kecamatan</label>
                                        <div class="col-sm-9">

                                            <!-- Select -->
                                            <select class="form-select" name="regencies_id" id="regencies_id" autocomplete="off" required>
                                                <option value="">Pilih Kecamatan...</option>
                                                @if (isset($user) ? $user->user_personal_information->regencies_id : ''))
                                                    <option value="{{ old('regencies_id', isset($user) ? $user->user_personal_information->regencies_id : '') }}" selected>{{ $user->user_personal_information->regencies->name }}</option>
                                                @endif
                                            </select>
                                            <!-- End Select -->

                                            @if($errors->has('regencies_id'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('regencies_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Form -->
                                    
                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="districts_id" class="col-sm-3 col-form-label form-label">Kabupaten/Kota</label>
                                        <div class="col-sm-9">

                                            <!-- Select -->
                                            <select class="form-select" name="districts_id" id="districts_id" autocomplete="off" required>
                                                <option value="">Pilih Kabupaten/Kota...</option>
                                                @if (isset($user) ? $user->user_personal_information->districts_id : ''))
                                                    <option value="{{ old('districts_id', isset($user) ? $user->user_personal_information->districts_id : '') }}" selected>{{ $user->user_personal_information->districts->name }}</option>
                                                @endif
                                            </select>
                                            <!-- End Select -->

                                            @if($errors->has('districts_id'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('districts_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="address" class="col-sm-3 col-form-label form-label">Alamat</label>

                                        <div class="col-sm-9">
                                            <textarea name="address" class="form-control" id="address" rows="5" style="resize: none" placeholder="Alamat" required autocomplete="off">{{ old('address') ?? $user->user_personal_information->address  }}</textarea>

                                            @if($errors->has('address'))
                                                <span style="font-style: bold; color: red;">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                     <div class="d-flex justify-content-end gap-3">
                                        <a class="btn btn-white" href="{{ route('profile.index') }}" onclick="return confirm('Anda yakin ingin menutup halaman ini? , Setiap perubahan yang Anda buat tidak akan disimpan.')">
                                            Cancel
                                        </a>
                                        <button type="submit" class="btn btn-gold" onclick="return confirm('Apakah Anda yakin ingin menyimpan data ini ?')">Simpan</button>
                                    </div>
                                </form>

                            </div>
                            <!-- End Body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
            
@endsection

@push('after-style')
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush


@push('after-script')

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
          $('#birth_date').datetimepicker({
              format: 'DD/MM/YYYY'
          });
    </script> 

    <script>
        // auto populate dropdown - regencies
        $(document).ready(function(){
            // provinces Change
            $('#provinces_id').change(function(){
                // provinces id
                var id = $(this).val();
                // Empty the dropdown
                $('#regencies_id').find('option').not(':first').remove();
                $('#districts_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '{{ URL::to("/select/regency/profile") }}'+'/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null)
                        {
                            len = response['data'].length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++)
                            {
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#regencies_id").append(option);
                            }
                        }
                    }
                });
            });
        });
        // auto populate dropdown - districts
        $(document).ready(function(){
            // regencies Change
            $('#regencies_id').change(function(){
                // regencies id
                var id = $(this).val();
                // Empty the dropdown
                $('#districts_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '{{ URL::to("/select/district/profile") }}'+'/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null)
                        {
                            len = response['data'].length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++)
                            {
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#districts_id").append(option);
                            }
                        }
                    }
                });
            });
        });
    </script>

@endpush