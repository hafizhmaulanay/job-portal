@extends('layouts.default')

{{-- set section --}}
@section('title', 'Keamanan - Profile | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Keamanan</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Akun</li>
                                <li class="breadcrumb-item active" aria-current="page">Keamanan</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Back to home</a>
                        </div>

                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-default">
                                <i class="bi-list"></i>
                            </span>
                            <span class="navbar-toggler-toggled">
                                <i class="bi-x"></i>
                            </span>
                        </button>
                        <!-- End Responsive Toggle Button -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">
            <div class="row">
                <div class="col-lg-3">
                   
                    @include('includes.profile.menu')

                </div>
                <!-- End Col -->

                <div class="col-lg-9">
                    <div class="d-grid gap-3 gap-lg-6">

                         <!-- Card -->
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h5 class="card-header-title">Ubah Email</h5>

                                 <!-- Error only email -->
                                @if ($errors->has('email'))
                                    <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                                        <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                                        <ul class="mt-3 mb-0">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                @endif
                                <!-- End Error -->
                            </div>

                            <!-- Body -->
                            <div class="card-body">

                                <form action="{{ route('update.email.security.profile') }}" method="POST">

                                    @csrf
                                    @method('PUT')
                                    
                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="email" class="col-sm-3 col-form-label form-label">Email Baru</label>

                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" placeholder="Masukkan email baru anda" aria-label="Masukkan email baru anda" required autocomplete="off">
                                            <small class="form-text">Email akan berubah ketika Anda sudah klik link verifikasi yang dikirimkan ke email baru Anda.</small>
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn btn-gold" onclick="return confirm('Apakah anda yakin ingin mengubah email anda?')">Ubah Email</button>
                                    </div>
                                </form>
                            </div>
                            <!-- End Body -->
                        </div>
                        <!-- End Card -->

                        <!-- Card -->
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h5 class="card-header-title">Ubah Password</h5>

                                    <!-- Error only password-->
                                    @if ($errors->has('password'))
                                        <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                                            <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                                            <ul class="mt-3 mb-0">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    @endif
                                    <!-- End Error -->
                            </div>

                            <!-- Body -->
                            <div class="card-body">
                                <!-- Form -->
                                <form method="POST" action="{{ route('update.password.security.profile') }}">

                                    @csrf
                                    @method('PUT')

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="password" class="col-sm-3 col-form-label form-label">Password Baru</label>

                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan password baru" aria-label="Masukkan password baru" required autocomplete="off">
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="password_confirmation" class="col-sm-3 col-form-label form-label">Konfirmasi Password Baru</label>

                                        <div class="col-sm-9">
                                            <div class="mb-3">
                                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi password baru" aria-label="Konfirmasi password baru" autocomplete="off" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Form -->

                                    <div class="d-flex justify-content-end gap-3">
                                        <button type="submit" class="btn btn-gold" onclick="return confirm('Apakah anda yakin ingin mengubah password?')">Ubah Password</button>
                                    </div>
                                </form>
                                <!-- End Form -->
                            </div>
                            <!-- End Body -->
                        </div>
                        <!-- End Card -->

                    </div>
                </div>
            </div>
        </div>
    </main>
            
@endsection

@push('after-style')
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush


@push('after-script')

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
          $('#birth_date').datetimepicker({
              format: 'DD/MM/YYYY'
          });
    </script> 

    <script>
        // auto populate dropdown - regencies
        $(document).ready(function(){
            // provinces Change
            $('#provinces_id').change(function(){
                // provinces id
                var id = $(this).val();
                // Empty the dropdown
                $('#regencies_id').find('option').not(':first').remove();
                $('#districts_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '{{ URL::to("/select/regency/profile") }}'+'/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null)
                        {
                            len = response['data'].length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++)
                            {
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#regencies_id").append(option);
                            }
                        }
                    }
                });
            });
        });
        // auto populate dropdown - districts
        $(document).ready(function(){
            // regencies Change
            $('#regencies_id').change(function(){
                // regencies id
                var id = $(this).val();
                // Empty the dropdown
                $('#districts_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '{{ URL::to("/select/district/profile") }}'+'/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null)
                        {
                            len = response['data'].length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++)
                            {
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#districts_id").append(option);
                            }
                        }
                    }
                });
            });
        });
    </script>

@endpush