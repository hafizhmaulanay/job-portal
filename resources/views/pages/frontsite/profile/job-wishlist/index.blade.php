@extends('layouts.default')

{{-- set section --}}
@section('title', 'Lowongan Tersimpan - Profile | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Lowongan Tersimpan</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Akun</li>
                                <li class="breadcrumb-item active" aria-current="page">Lowongan Tersimpan</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Back to home</a>
                        </div>

                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-default">
                                <i class="bi-list"></i>
                            </span>
                            <span class="navbar-toggler-toggled">
                                <i class="bi-x"></i>
                            </span>
                        </button>
                        <!-- End Responsive Toggle Button -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">
            <div class="row">
                <div class="col-lg-3">
                    
                    @include('includes.profile.menu')

                </div>
                <!-- End Col -->

                <div class="col-lg-9">
                    <!-- Card -->
                    <div class="card">
                        <div class="card-header border-bottom">
                            <h5 class="card-title">Lowongan Tersimpan</h5>
                        </div>

                        <!-- Body -->
                        <div class="card-body">
                            <div class="row row-cols-1 row-cols-sm-1 mb-2">

                                @forelse ($job_wishlist as $job_wishlist_item)

                                    @php
                                        $company_is_active = $job_wishlist_item->job_vacancy->company->status == 1;
                                        $job_vacancy_is_active = $job_wishlist_item->job_vacancy->status == 1;
                                        $job_vacancy_is_valid = $job_wishlist_item->job_vacancy()->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('end_date', '>=', date('Y-m-d'))->count() > 0;
                                        $job_vacancy_contract_is_valid = $job_wishlist_item->job_vacancy->company()->whereHas('company_contract_history', function($query) {
                                            $query->whereHas('company_contract', function($query) {
                                                $query->where('status', 1)->whereDate('end_period', '>=', date('Y-m-d'));
                                            });
                                        })->count() > 0;
                                    @endphp

                                    <div class="col mb-5">
                                        <!-- Card -->
                                        <a href="{{ route('job_vacancy.show', $job_wishlist_item->job_vacancy->uuid) }}" class="card card-bordered card-transition h-100" target="_blank" rel="noopener noreferrer">
                                            <!-- Card Body -->
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col">
                                                        <!-- Media -->
                                                        <div class="d-flex align-items-center">
                                                            <div class="flex-shrink-0">
                                                                <img src="
                                                                @if ($job_wishlist_item->job_vacancy->company->image != "")
                                                                    {{ $job_wishlist_item->job_vacancy->company->image_url }}
                                                                @else
                                                                    {{ asset('/back-design/clients/default/image-not-found.svg') }}
                                                                @endif "
                                                                alt="{{ $job_wishlist_item->job_vacancy->company->name ?? 'job vacancy logo' }}" class="avatar avatar-sm avatar-4x3">
                                                            </div>

                                                            <div class="flex-grow-1 ms-3">
                                                                <h6 class="card-title">
                                                                    <span class="text-dark">{{ isset($job_wishlist_item->job_vacancy->company->name) ? Str::title($job_wishlist_item->job_vacancy->company->name)  : '' }}</span>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                        <!-- End Media -->
                                                    </div>
                                                    <!-- End Col -->

                                                    <div class="col-auto">
                                                        <div class="d-grid d-md-flex gap-2 align-items-md-center">

                                                            @unless ($company_is_active && $job_vacancy_is_active && $job_vacancy_is_valid && $job_vacancy_contract_is_valid)
                                                                <div class="btn btn-sm bg-soft-dark" style="cursor: default">
                                                                    Closed
                                                                </div>
                                                            @endunless

                                                            <form action="{{ route('job_wishlist.destroy', $job_wishlist_item->job_vacancy->id) }}" method="POST"
                                                                onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini?');">
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <button type="submit" class="btn btn-sm btn-soft-danger">
                                                                    <i class="bi-trash me-1"></i> Hapus
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <!-- End Col -->
                                                </div>
                                                <!-- End Row -->

                                                <h3 class="card-title">
                                                    <span class="text-dark">{{ isset($job_wishlist_item->job_vacancy->position->name) ? Str::title($job_wishlist_item->job_vacancy->position->name) : '' }}</span>
                                                </h3>

                                            </div>
                                            <!-- End Card Body -->

                                            <!-- Card Footer -->
                                            <div class="card-footer pt-0">
                                                <ul class="list-inline list-separator small text-body">
                                                <li class="list-inline-item"><i class="bi bi-briefcase-fill pe-2"></i> {{ isset($job_wishlist_item->job_vacancy->experience_year) ? 'Min ' . $job_wishlist_item->job_vacancy->experience_year . ' tahun' : 'Tidak ada' }}</li>
                                                <li class="list-inline-item"><i class="bi bi-geo-alt-fill pe-2"></i> {{ $job_wishlist_item->job_vacancy->provinces->name ?? '' }}</li>
                                                <li class="list-inline-item"><i class="bi bi-clock-fill pe-2"></i> Posted {{ $job_wishlist_item->job_vacancy->created_at->diffForHumans() }}</li>
                                                </ul>
                                            </div>
                                            <!-- End Card Footer -->
                                        </a>
                                        <!-- End Card -->
                                    </div>
                                    <!-- End Col -->

                                @empty
                                    <!-- Content -->
                                    <div class="container col my-5 text-center">
                                        <div class="mb-3">
                                            <img class="img-fluid" src="{{ asset('front-design/assets/img/illustrations/sorry.png') }}" alt="Image Description" style="width: 12rem;">
                                        </div>

                                        <div class="mb-4">
                                            <h2 class="my-2">Oops!</h2>
                                            <p class="fs-4">Lowongan pekerjaan tidak ditemukan.</p>
                                        </div>
                                    </div>
                                @endforelse

                            </div>
                            <!-- End Row -->
                        </div>
                        <!-- End Body -->

                        <!-- Pagination -->
                        <div class="table-responsive">
                            <div class="text-center mb-3">
                                @if ($job_wishlist->hasPages())
                                    <h4 class="card-title">Pagination Page</h4>
                                    <p class="mt-1"><code class="text-dark">Page
                                        {{ $job_wishlist->currentPage() }} of {{ $job_wishlist->lastPage() }}</code>
                                        All data <code class="text-dark">{{ $job_wishlist->total() }}</code>
                                    </p>
                                @endif
                                <nav aria-label="Page navigation">
                                    @if ($job_wishlist->hasPages())
                                        <ul class="pagination justify-content-center pagination-round">

                                            {{-- Previous Page Link --}}
                                            @if ($job_wishlist->onFirstPage())
                                                <li class="page-item disabled">
                                                    <a class="page-link" href="#"
                                                        aria-label="Previous">
                                                        <i class="bi-chevron-double-left small"></i>
                                                    </a>
                                                </li>
                                            @else
                                                <li class="page-item">
                                                    <a class="page-link"
                                                        href="{{ $job_wishlist->previousPageUrl() }}"
                                                        aria-label="Previous">
                                                        <i class="bi-chevron-double-left small"></i>
                                                    </a>
                                                </li>
                                            @endif

                                            @if ($job_wishlist->currentPage() > 2)
                                                <li class="page-item"><a class="page-link"
                                                        href="{{ $job_wishlist->url(1) }}">1</a></li>
                                            @endif

                                            @if ($job_wishlist->currentPage() > 3)
                                                <li class="page-item disabled"><a class="page-link"
                                                        href="#">...</a></li>
                                            @endif

                                            @foreach (range(1, $job_wishlist->lastPage()) as $i)
                                                @if ($i >= $job_wishlist->currentPage() - 1 && $i <= $job_wishlist->currentPage() + 1)
                                                    @if ($i == $job_wishlist->currentPage())
                                                        <li class="page-item active"><span
                                                                class="page-link">{{ $i }}</span>
                                                        </li>
                                                    @else
                                                        <li class="page-item"><a class="page-link"
                                                                href="{{ $job_wishlist->url($i) }}">{{ $i }}</a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endforeach

                                            @if ($job_wishlist->currentPage() < $job_wishlist->lastPage() - 3)
                                                <li class="page-item disabled"><a class="page-link"
                                                        href="#">...</a></li>
                                            @endif

                                            @if ($job_wishlist->currentPage() < $job_wishlist->lastPage() - 2)
                                                <li class="page-item"><a class="page-link"
                                                        href="{{ $job_wishlist->url($job_wishlist->lastPage()) }}">{{ $job_wishlist->lastPage() }}</a>
                                                </li>
                                            @endif

                                            {{-- Next Page Link --}}
                                            @if ($job_wishlist->hasMorePages())
                                                <li class="page-item">
                                                    <a class="page-link"
                                                        href="{{ $job_wishlist->nextPageUrl() }}"
                                                        aria-label="Next">
                                                        <i class="bi-chevron-double-right small"></i>
                                                    </a>
                                                </li>
                                            @else
                                                <li class="page-item disabled">
                                                    <a class="page-link" href="#" aria-label="Next">
                                                        <i class="bi-chevron-double-right small"></i>
                                                    </a>
                                                </li>
                                            @endif

                                        </ul>
                                    @endif
                                </nav>
                            </div>
                        </div>
                        <!-- End Pagination -->

                        {{-- card foooter --}}
                        <a class="card-footer card-link text-center border-top" href="{{ route('home') }}">Cari Lowongan</a>   
                    </div>
                    <!-- End Card -->
                </div>
                <!-- End Col -->
            </div>
        </div>
    </main>
            
@endsection