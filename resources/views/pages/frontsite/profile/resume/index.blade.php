@extends('layouts.default')

{{-- set section --}}
@section('title', 'Resume - Profile | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Resume</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Akun</li>
                                <li class="breadcrumb-item active" aria-current="page">Resume</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Back to home</a>
                        </div>

                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-default">
                                <i class="bi-list"></i>
                            </span>
                            <span class="navbar-toggler-toggled">
                                <i class="bi-x"></i>
                            </span>
                        </button>
                        <!-- End Responsive Toggle Button -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-t-1 content-space-b-3 content-space-b-md-5 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">
            <div class="row">
                <div class="col-lg-3">
                    
                    @include('includes.profile.menu')

                </div>
                <!-- End Col -->

                <div class="col-lg-9">
                    <!-- Card -->
                    <div class="card">
                        <div class="card-header border-bottom">
                            <h5 class="card-title">Resume</h5>

                             <!-- Error -->
                            @if ($errors->any())
                                <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                                    <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                                    <ul class="mt-3 mb-0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <!-- End Error -->
                        </div>

                        <!-- Body -->
                        <div class="card-body">
                            
                            @if ($user->user_resume)
                                <!-- Form -->
                                <div class="row mb-4 align-items-center">
                                    <label for="resume" class="col-sm-3 col-form-label form-label">Resume File</label>

                                    <div class="col-sm-9">
                                        <ul class="list-checked list-checked-soft-bg-success mb-0">
                                            <li class="list-checked-item">
                                                {{ $user->user_resume->resume_name }}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Form -->
                            @else
                                <!-- Form -->
                                <form action="{{ route('resume.update', $user->id) }}" method="POST" enctype="multipart/form-data">

                                    @csrf
                                    @method('PUT')

                                    <!-- Form -->
                                    <div class="row mb-4">
                                        <label for="resume" class="col-sm-3 col-form-label form-label">Resume File</label>

                                        <div class="col-sm-9">
                                            <input class="js-file-attach form-control" name="resume" type="file" id="basicFormFile" required 
                                            data-hs-file-attach-options='{
                                                    "allowTypes": [".pdf"],
                                                    "maxFileSize": "5000"
                                                }'>
                                        </div>
                                    </div>
                                    <!-- End Form -->
                                    
                                    <div class="d-flex justify-content-end gap-3 mt-3">
                                        <a class="btn btn-white" href="{{ route('resume.profile') }}" onclick="return confirm('Anda yakin ingin menutup halaman ini? , Setiap perubahan yang Anda buat tidak akan disimpan.')">
                                            Batal
                                        </a>
                                        <button type="submit" class="btn btn-gold" onclick="return confirm('Apakah Anda yakin ingin menyimpan data ini?')">Simpan</button>
                                    </div>
                                </form>
                                <!-- End Form -->
                            @endif        
                        </div>
                        <!-- End Body -->

                        <!-- Footer -->
                        @if ($user->user_resume)
                            <div class="card-footer pt-0">
                                <div class="d-flex justify-content-end gap-3">
                                    <form action="{{ route('resume.destroy', $user->id) }}" method="POST"
                                        onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini?');">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                    <a href="{{ asset('storage/'.$user->user_resume->resume_file) }}" class="btn btn-primary" rel="noopener noreferrer" target="_blank">View Resume</a>
                                </div>
                            </div>                       
                        @endif
                        <!-- End Footer -->

                    </div>
                    <!-- End Card -->
                </div>
                <!-- End Col -->
            </div>
        </div>
    </main>
            
@endsection

@push('after-style')
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush


@push('after-script')
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
          $('#end_date').datetimepicker({
              format: 'DD/MM/YYYY'
          });
    </script> 

@endpush