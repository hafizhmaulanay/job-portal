@extends('layouts.default')

{{-- set section --}}
@section('title', 'Edit Pengalaman Kerja - Profile | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Edit Pengalaman Kerja</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Akun</li>
                                <li class="breadcrumb-item">Pengalaman Kerja</li>
                                <li class="breadcrumb-item active" aria-current="page">Edit</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Back to home</a>
                        </div>

                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-default">
                                <i class="bi-list"></i>
                            </span>
                            <span class="navbar-toggler-toggled">
                                <i class="bi-x"></i>
                            </span>
                        </button>
                        <!-- End Responsive Toggle Button -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">
            <div class="row">
                <div class="col-lg-3">
                    
                    @include('includes.profile.menu')

                </div>
                <!-- End Col -->

                <div class="col-lg-9">
                    <!-- Card -->
                    <div class="card">
                        <div class="card-header border-bottom">
                             <div class="row align-items-center">
                                <div class="col-sm mb-2 mb-sm-0">
                                    <h5 class="card-header-title mb-2 lh-base">Edit Pengalaman Kerja</h5>
                                </div>
                            </div>
                            <!-- End Row -->

                            <!-- Error -->
                            @if ($errors->any())
                                <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                                    <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                                    <ul class="mt-3 mb-0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <!-- End Error -->
                        </div>

                        <!-- Body -->
                        <div class="card-body">

                             <!-- Form -->
                            <form action="{{ route('work_experience.update', $work_experience->id) }}" method="POST">

                                @csrf
                                @method('PUT')

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="position" class="col-sm-3 col-form-label form-label">Posisi</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="position" id="position" placeholder="Posisi" aria-label="Posisi" value="{{ old('position', isset($work_experience) ? $work_experience->position : '') }}" autocomplete="off" required>

                                        @if($errors->has('position'))
                                            <span style="font-style: bold; color: red;">{{ $errors->first('position') }}</span>
                                        @endif

                                    </div>
                                </div>
                                <!-- End Form -->

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="company" class="col-sm-3 col-form-label form-label">Perusahaan</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="company" id="company" placeholder="Perusahaan" aria-label="Perusahaan" value="{{ old('company', isset($work_experience) ? $work_experience->company : '') }}" autocomplete="off" required>

                                        @if($errors->has('company'))
                                            <span style="font-style: bold; color: red;">{{ $errors->first('company') }}</span>
                                        @endif

                                    </div>
                                </div>
                                <!-- End Form -->

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="start_period" class="col-sm-3 col-form-label form-label">Tanggal Mulai</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-lg" value="{{ old('start_period', isset($work_experience->start_period) ? $work_experience->start_period->format('d/m/Y') : '') }}" name="start_period" id="start_period" autocomplete="off" placeholder="Tanggal Mulai" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>

                                        @if($errors->has('start_period'))
                                            <span style="font-style: bold; color: red;">{{ $errors->first('start_period') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- End Form -->

                                <!-- Form -->
                                <div class="row mb-4">
                                    <label for="end_period" class="col-sm-3 col-form-label form-label">Tanggal Berakhir</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-lg" value="{{ old('start_period', isset($work_experience->end_period) ? $work_experience->end_period->format('d/m/Y') : '') }}" name="end_period" id="end_period" autocomplete="off" placeholder="Tanggal Berakhir" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>
                                        
                                        @if($errors->has('end_period'))
                                            <span style="font-style: bold; color: red;">{{ $errors->first('end_period') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- End Form -->

                                
                                <div class="d-flex justify-content-end gap-3 mt-3">
                                    <a class="btn btn-white" href="{{ route('work_experience.profile') }}" onclick="return confirm('Anda yakin ingin menutup halaman ini? , Setiap perubahan yang Anda buat tidak akan disimpan.')">
                                        Batal
                                    </a>
                                    <button type="submit" class="btn btn-gold" onclick="return confirm('Apakah Anda yakin ingin menyimpan data ini?')">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                            <!-- End Form -->
                    
                        </div>
                        <!-- End Body -->
                    </div>
                    <!-- End Card -->
                
                </div>
                <!-- End Col -->

            </div>
        </div>
    </main>
            
@endsection

@push('after-style')
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush


@push('after-script')
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
          $('#start_period').datetimepicker({
              format: 'DD/MM/YYYY'
          });
          $('#end_period').datetimepicker({
              format: 'DD/MM/YYYY'
          });
    </script>

@endpush