@extends('layouts.default')

{{-- set section --}}
@section('title', 'Pengalaman Kerja - Profile | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Pengalaman Kerja</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Akun</li>
                                <li class="breadcrumb-item active" aria-current="page">Pengalaman Kerja</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Back to home</a>
                        </div>

                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-default">
                                <i class="bi-list"></i>
                            </span>
                            <span class="navbar-toggler-toggled">
                                <i class="bi-x"></i>
                            </span>
                        </button>
                        <!-- End Responsive Toggle Button -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">
            <div class="row">
                <div class="col-lg-3">
                    
                    @include('includes.profile.menu')

                </div>
                <!-- End Col -->

                <div class="col-lg-9">
                    <!-- Card -->
                    <div class="card">
                        <div class="card-header border-bottom">
                            <div class="row align-items-center">
                                <div class="col-sm mb-2 mb-sm-0">
                                    <h5 class="card-header-title mb-2 lh-base">Pengalaman Kerja</h5>
                                </div>

                                <div class="col-sm-auto">
                                    <!-- Nav -->
                                    <div class="text-center">
                                        <ul class="nav nav-segment nav-pills" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link {{ $errors->isEmpty() ? 'active' : '' }}" id="nav-one-eg1-tab" href="#nav-one-eg1" data-bs-toggle="pill" data-bs-target="#nav-one-eg1" role="tab" aria-controls="nav-one-eg1" aria-selected="true">Daftar</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link {{ $errors->any() ? 'active' : '' }}" id="nav-two-eg1-tab" href="#nav-two-eg1" data-bs-toggle="pill" data-bs-target="#nav-two-eg1" role="tab" aria-controls="nav-two-eg1" aria-selected="false">Tambah</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- End Nav -->
                                </div>
                            </div>
                            <!-- End Row -->

                            <!-- Error -->
                            @if ($errors->any())
                                <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                                    <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                                    <ul class="mt-3 mb-0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <!-- End Error -->
                        </div>

                        <!-- Body -->
                        <div class="card-body">

                            <!-- Tab Content -->
                            <div class="tab-content">
                                <div class="tab-pane fade{{ $errors->isEmpty() ? ' show active' : '' }}" id="nav-one-eg1" role="tabpanel" aria-labelledby="nav-one-eg1-tab">

                                     <!-- Table -->
                                    <div class="table-responsive pb-5 pb-lg-10">
                                        <table id="default-table" class="table table-thead-bordered table-align-middle pt-3">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Posisi</th>
                                                    <th>Perusahaan</th>
                                                    <th style="text-align: center;">Aksi</th>
                                                </tr>
                                            </thead>
                            
                                            <tbody>

                                                @forelse ($work_experience as $work_experience_item)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}.</td>
                                                        <td>{{ $work_experience_item->position ?? '' }}</td>
                                                        <td>{{ $work_experience_item->company ?? '' }}</td>
                                                        <td style="text-align: center;">
                                                            <!-- Dropdown -->
                                                            <div class="btn-group">
                                                                <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButtonInfo" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    Aksi
                                                                </button>
                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonInfo">
                                                                    <a class="dropdown-item" href="#showModal" data-bs-toggle="modal" 
                                                                        data-bs-target="#showModal" data-remote="{{ route('work_experience.show', $work_experience_item->id) }}" data-title="Pengalaman Kerja Detail">
                                                                        Lihat
                                                                    </a>

                                                                    <a class="dropdown-item" href="{{ route('work_experience.edit', $work_experience_item->id) }}">
                                                                        Edit
                                                                    </a>

                                                                    <form action="{{ route('work_experience.destroy', $work_experience_item->id) }}" method="POST"
                                                                        onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini ?');">
                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        <input type="submit" class="dropdown-item" value="Hapus">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <!-- End Dropdown -->
                                                        </td>
                                                    </tr>
                                                @empty
                                                    {{-- Not Found --}}
                                                @endforelse
                            
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- End Table -->

                                    <hr>

                                    <div class="table-responsive">
                                        <div class="text-center mb-3">
                                            @if ($work_experience->hasPages())
                                                <h4 class="card-title">Pagination Page</h4>
                                                <p class="mt-1"><code class="text-dark">Page
                                                    {{ $work_experience->currentPage() }} of {{ $work_experience->lastPage() }}</code>
                                                    All data <code class="text-dark">{{ $work_experience->total() }}</code>
                                                </p>
                                            @endif
                                            <nav aria-label="Page navigation">
                                                @if ($work_experience->hasPages())
                                                    <ul class="pagination justify-content-center pagination-round">

                                                        {{-- Previous Page Link --}}
                                                        @if ($work_experience->onFirstPage())
                                                            <li class="page-item disabled">
                                                                <a class="page-link" href="#"
                                                                    aria-label="Previous">
                                                                    <i class="bi-chevron-double-left small"></i>
                                                                </a>
                                                            </li>
                                                        @else
                                                            <li class="page-item">
                                                                <a class="page-link"
                                                                    href="{{ $work_experience->previousPageUrl() }}"
                                                                    aria-label="Previous">
                                                                    <i class="bi-chevron-double-left small"></i>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if ($work_experience->currentPage() > 2)
                                                            <li class="page-item"><a class="page-link"
                                                                    href="{{ $work_experience->url(1) }}">1</a></li>
                                                        @endif

                                                        @if ($work_experience->currentPage() > 3)
                                                            <li class="page-item disabled"><a class="page-link"
                                                                    href="#">...</a></li>
                                                        @endif

                                                        @foreach (range(1, $work_experience->lastPage()) as $i)
                                                            @if ($i >= $work_experience->currentPage() - 1 && $i <= $work_experience->currentPage() + 1)
                                                                @if ($i == $work_experience->currentPage())
                                                                    <li class="page-item active"><span
                                                                            class="page-link">{{ $i }}</span>
                                                                    </li>
                                                                @else
                                                                    <li class="page-item"><a class="page-link"
                                                                            href="{{ $work_experience->url($i) }}">{{ $i }}</a>
                                                                    </li>
                                                                @endif
                                                            @endif
                                                        @endforeach

                                                        @if ($work_experience->currentPage() < $work_experience->lastPage() - 3)
                                                            <li class="page-item disabled"><a class="page-link"
                                                                    href="#">...</a></li>
                                                        @endif

                                                        @if ($work_experience->currentPage() < $work_experience->lastPage() - 2)
                                                            <li class="page-item"><a class="page-link"
                                                                    href="{{ $work_experience->url($work_experience->lastPage()) }}">{{ $work_experience->lastPage() }}</a>
                                                            </li>
                                                        @endif

                                                        {{-- Next Page Link --}}
                                                        @if ($work_experience->hasMorePages())
                                                            <li class="page-item">
                                                                <a class="page-link"
                                                                    href="{{ $work_experience->nextPageUrl() }}"
                                                                    aria-label="Next">
                                                                    <i class="bi-chevron-double-right small"></i>
                                                                </a>
                                                            </li>
                                                        @else
                                                            <li class="page-item disabled">
                                                                <a class="page-link" href="#" aria-label="Next">
                                                                    <i class="bi-chevron-double-right small"></i>
                                                                </a>
                                                            </li>
                                                        @endif

                                                    </ul>
                                                @endif
                                            </nav>
                                        </div>
                                    </div>


                                </div>

                                <div class="tab-pane fade{{ $errors->any() ? ' show active' : '' }}" id="nav-two-eg1" role="tabpanel" aria-labelledby="nav-two-eg1-tab">
                                    
                                     <!-- Form -->
                                    <form action="{{ route('work_experience.store') }}" method="POST">

                                        @csrf

                                        <!-- Form -->
                                        <div class="row mb-4">
                                            <label for="position" class="col-sm-3 col-form-label form-label">Posisi</label>

                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="position" id="position" placeholder="Posisi" aria-label="Posisi" value="{{ old('position') }}" autocomplete="off" required>

                                                @if($errors->has('position'))
                                                    <span style="font-style: bold; color: red;">{{ $errors->first('position') }}</span>
                                                @endif

                                            </div>
                                        </div>
                                        <!-- End Form -->

                                        <!-- Form -->
                                        <div class="row mb-4">
                                            <label for="company" class="col-sm-3 col-form-label form-label">Perusahaan</label>

                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="company" id="company" placeholder="Perusahaan" aria-label="Perusahaan" value="{{ old('company') }}" autocomplete="off" required>

                                                @if($errors->has('company'))
                                                    <span style="font-style: bold; color: red;">{{ $errors->first('company') }}</span>
                                                @endif

                                            </div>
                                        </div>
                                        <!-- End Form -->

                                        <!-- Form -->
                                        <div class="row mb-4">
                                            <label for="start_period" class="col-sm-3 col-form-label form-label">Tanggal Mulai</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control form-control-lg" value="{{ old('start_period') }}" name="start_period" id="start_period" autocomplete="off" placeholder="Tanggal Mulai" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>

                                                @if($errors->has('start_period'))
                                                    <span style="font-style: bold; color: red;">{{ $errors->first('start_period') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- End Form -->

                                        <!-- Form -->
                                        <div class="row mb-4">
                                            <label for="end_period" class="col-sm-3 col-form-label form-label">Tanggal Berakhir</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control form-control-lg" value="{{ old('end_period') }}" name="end_period" id="end_period" autocomplete="off" placeholder="Tanggal Berakhir" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>
                                                
                                                @if($errors->has('end_period'))
                                                    <span style="font-style: bold; color: red;">{{ $errors->first('end_period') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- End Form -->

                                        
                                        <div class="d-flex justify-content-end gap-3 mt-3">
                                            <a class="btn btn-white" href="{{ route('work_experience.profile') }}" onclick="return confirm('Anda yakin ingin menutup halaman ini? , Setiap perubahan yang Anda buat tidak akan disimpan.')">
                                                Batal
                                            </a>
                                            <button type="submit" class="btn btn-gold" onclick="return confirm('Apakah Anda yakin ingin menyimpan data ini ?')">
                                                Simpan
                                            </button>
                                        </div>
                                    </form>
                                    <!-- End Form -->

                                </div>
                            </div>
                            <!-- End Tab Content -->
                        </div>
                        <!-- End Body -->
                    </div>
                    <!-- End Card -->
                
                </div>
                <!-- End Col -->

            </div>
        </div>
    </main>
            
@endsection

@push('after-style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush


@push('after-script')
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
    
    <script>
        jQuery(document).ready(function($) {
            $('#showModal').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget);
                var modal = $(this);

                modal.find('.modal-body').load(button.data("remote"));
                modal.find('.modal-title').html(button.data("title"));
            });
        });

        $('#default-table').DataTable({
            "order": [],
            "ordering": false,
            "paging": true,
            "lengthMenu": [
            [5, 10, 25, 50, 100, -1],
            [5, 10, 25, 50, 100, "All"]
            ],
            "pageLength": 10
        });
	</script>

    <script>
          $('#start_period').datetimepicker({
              format: 'DD/MM/YYYY'
          });
          $('#end_period').datetimepicker({
              format: 'DD/MM/YYYY'
          });
    </script> 

     <!-- Modal -->
    <div id="showModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="showModalTitle">Detail Lamaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="spinner-border text-primary" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

@endpush