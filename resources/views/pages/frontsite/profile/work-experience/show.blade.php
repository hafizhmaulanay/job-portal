<!-- Table -->
<table class="table table-vertical-border-striped">
    <tbody>
        <tr>
            <th>Posisi :</th>
            <td>{{ isset($work_experience->position) ? $work_experience->position : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Perusahaan :</th>
            <td>{{ isset($work_experience->company) ? $work_experience->company : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Tanggal :</th>
            <td>{{ isset($work_experience->start_period) ? $work_experience->start_period->format('d, F Y') : 'N/A' }} - {{ isset($work_experience->end_period) ? $work_experience->end_period->format('d, F Y') : 'N/A' }}</td>
        </tr>
    </tbody>
</table>
<!-- End Table -->