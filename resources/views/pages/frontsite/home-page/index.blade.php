@extends('layouts.default')
    
{{-- set content --}}
@section('content')
<!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <form method="GET" action="{{ route('job-vacancy.filter') }}">
      <!-- Hero -->
      <div class="gradient-x-three-sm-primary">
        <div class="container content-space-2">
            <!-- Input Card -->
            <div class="input-card input-card-sm mb-3">
              <div class="input-card-form">
                <label for="keyword" class="form-label visually-hidden">Pekerjaan, kata kunci atau perusahaan</label>
                <div class="input-group input-group-merge">
                  <span class="input-group-prepend input-group-text">
                    <i class="bi-search"></i>
                  </span>
                  <input type="text" class="form-control" name="keyword" id="keyword" value="{{ request('keyword') ?? '' }}" placeholder="Pekerjaan, kata kunci atau perusahaan" aria-label="Pekerjaan, kata kunci atau perusahaan" minlength="3" autocomplete="off" required>
                </div>
              </div>
  
              <div class="input-card-form">
                <label for="cityForm" class="form-label visually-hidden">Lokasi</label>
               
                <div class="input-group input-group-merge" style="overflow: visible">
                  <span class="input-group-prepend input-group-text">
                    <i class="bi-geo-alt"></i>
                  </span>
                  <!-- Select -->
                  <div class="tom-select-custom w-100">
                    <select class="js-select form-select" name="province_id" autocomplete="off"
                            data-hs-tom-select-options='{
                              "width": "100%",
                              "dropdownLeft": true,
                              "searchInDropdown": false,
                              "hidePlaceholderOnSearch": true,
                              "placeholder": "Pilih Lokasi..."
                            }'>
                      <option value="">Pilih Lokasi...</option>
                      @foreach ($province as $id => $name)
                        <option value="{{ $id }}" {{ request('province_id') == $id ? 'selected' : '' }}>{{ $name }}</option>
                      @endforeach
  
                    </select>
                  </div>
                  <!-- End Select -->
                </div>
              </div>
  
              <button type="submit" class="btn btn-gold">Search</button>
  
              @if(request('keyword') || request('province_id') || request('level_of_education_id') || request('job_type_id'))
                <a href="{{ route('home') }}" class="btn btn-outline-seconday">
                    Reset
                </a>   
              @endif
            </div>
            <!-- End Input Card -->
  
          {{-- <div class="row align-items-center">
            <div class="col-md-auto mb-3 mb-lg-0">
              <h6 class="mb-1">Limit search to:</h6>
            </div>
            <!-- End Col -->
  
            <div class="col-md mb-3 mb-lg-0">
              <!-- Check -->
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="jobSearchToCheckbox1" value="option1" checked>
                <label class="form-check-label" for="jobSearchToCheckbox1">Job title</label>
              </div>
              <!-- End Check -->
  
              <!-- Check -->
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="jobSearchToCheckbox2" value="option2">
                <label class="form-check-label" for="jobSearchToCheckbox2">Skills</label>
              </div>
              <!-- End Check -->
  
              <!-- Check -->
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="jobSearchToCheckbox3" value="option3">
                <label class="form-check-label" for="jobSearchToCheckbox3">Companies</label>
              </div>
              <!-- End Check -->
  
              <!-- Check -->
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="jobSearchToCheckbox4" value="option4">
                <label class="form-check-label" for="jobSearchToCheckbox4">Field of study</label>
              </div>
              <!-- End Check -->
            </div>
  
            <div class="col-md-auto">
              <!-- Switch -->
              <div class="form-check form-switch">
                <input class="form-check-input" type="checkbox" id="remoteOnlySwitch">
                <label class="form-check-label" for="remoteOnlySwitch">Remote only</label>
              </div>
              <!-- End Switch -->
            </div>
            <!-- End Col -->
          </div> --}}
          <!-- End Row -->
        </div>
      </div>
      <!-- End Hero -->

      <!-- Card Grid -->
      <div class="container content-space-t-1 content-space-t-md-2 content-space-b-2 content-space-b-lg-3">
        <div class="row">

          <div class="col-lg-12">
            <div class="row align-items-center mb-5">
              <div class="col-sm-6 mb-3 mb-sm-0">
                <h3 class="mb-0">
                  @if ($job_vacancy->total() > 0)
                    Page {{ number_format($job_vacancy->currentPage()) }} - {{ number_format($job_vacancy->lastPage()) }} dari {{ number_format($job_vacancy->total()) }} lowongan
                  @else
                    Tidak ada lowongan yang ditemukan
                  @endif
                </h3>
              </div>

              <div class="col-sm-6">
                <div class="d-sm-flex justify-content-sm-end align-items-center">
                  <!-- Select -->
                  <div class="mb-2 mb-sm-0 me-sm-2 flex-fill">
                    <div class="tom-select-custom">
                      <select class="js-select form-select" autocomplete="off"
                              name="level_of_education_id" onchange="this.form.submit()"
                              data-hs-tom-select-options='{
                                "searchInDropdown": false,
                                "hidePlaceholderOnSearch": true,
                                "placeholder": "Kualifikasi Pendidikan"
                              }'>
                        <option value="">Kualifikasi Pendidikan</option>
                        @foreach ($level_of_education as $id => $name)
                          <option value="{{ $id }}" {{ request('level_of_education_id') == $id ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <!-- End Select -->

                  <!-- Select -->
                  <div class="mb-2 mb-sm-0 me-sm-2 flex-fill">
                      <div class="tom-select-custom">
                      <select class="js-select form-select" autocomplete="off"
                              name="job_type_id" onchange="this.form.submit()"
                              data-hs-tom-select-options='{
                                "searchInDropdown": false,
                                "hidePlaceholderOnSearch": true,
                                "placeholder": "Tipe Pekerjaan"
                              }'>
                        <option value="">Tipe Pekerjaan</option>
                        @foreach ($job_type as $id => $name)
                          <option value="{{ $id }}" {{ request('job_type_id') == $id ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <!-- End Select -->
                </div>
              </div>
            </div>
            <!-- End Row -->

            <div class="row row-cols-1 row-cols-sm-2 mb-5">

              @forelse ($job_vacancy as $job_vacancy_item)
                  <div class="col mb-5">
                    <!-- Card -->
                    <a class="card card-bordered card-transition h-100" href="{{ route('job_vacancy.show', $job_vacancy_item->uuid) }}" target="_blank" rel="noopener noreferrer">
                      <!-- Card Body -->
                      <div class="card-body">
                        <div class="row mb-3">
                          <div class="col">
                            <!-- Media -->
                            <div class="d-flex align-items-center">
                              <div class="flex-shrink-0">
                                <img src="
                                  @if ($job_vacancy_item->company->image != "")
                                      {{ $job_vacancy_item->company->image_url }}
                                  @else
                                      {{ asset('/back-design/clients/default/image-not-found.svg') }}
                                  @endif "
                                  alt="{{ $job_vacancy_item->company->user->name ?? 'job vacancy logo' }}" class="avatar avatar-sm avatar-4x3">
                              </div>

                              <div class="flex-grow-1 ms-3">
                                <h6 class="card-title">
                                  <span class="text-dark">{{ isset($job_vacancy_item->company->user->name) ? Str::title($job_vacancy_item->company->user->name)  : '' }}</span>
                                </h6>
                              </div>
                            </div>
                            <!-- End Media -->
                          </div>
                          <!-- End Col -->
                        </div>
                        <!-- End Row -->

                        <h3 class="card-title text-inherit">
                          <span class="text-dark">{{ isset($job_vacancy_item->position->name) ? Str::title($job_vacancy_item->position->name) : '' }}</span>
                        </h3>

                      </div>
                      <!-- End Card Body -->

                      <!-- Card Footer -->
                      <div class="card-footer pt-0">
                        <ul class="list-inline list-separator small text-body">
                          <li class="list-inline-item"><i class="bi bi-briefcase-fill pe-2"></i> {{ isset($job_vacancy_item->experience_year) ? 'Min ' . $job_vacancy_item->experience_year . ' tahun' : 'Tidak ada' }}</li>
                          <li class="list-inline-item"><i class="bi bi-geo-alt-fill pe-2"></i> {{ $job_vacancy_item->provinces->name ?? '' }}</li>
                          <li class="list-inline-item"><i class="bi bi-clock-fill pe-2"></i> Posted {{ $job_vacancy_item->created_at->diffForHumans() }}</li>
                        </ul>
                      </div>
                      <!-- End Card Footer -->
                    </a>
                    <!-- End Card -->
                  </div>
                  <!-- End Col -->
              @empty
                  <!-- Content -->
                  <div class="container col my-5 text-center">
                    <div class="mb-3">
                      <img class="img-fluid" src="{{ asset('front-design/assets/img/illustrations/sorry.png') }}" alt="Image Description" style="width: 18rem;">
                    </div>

                    <div class="mb-4">
                      <h2 class="my-2">Oops!</h2>
                      <p class="fs-4">Lowongan pekerjaan tidak ditemukan.</p>
                    </div>
                  </div>
              @endforelse

            </div>
            <!-- End Row -->

            <!-- Pagination -->
            {{ $job_vacancy->links('pagination::bootstrap-4') }}
            <!-- End Pagination -->
          </div>
          <!-- End Col -->

        </div>
        <!-- End Row -->
      </div>
      <!-- End Card Grid -->
    </form>
  </main>
<!-- ========== END MAIN CONTENT ========== -->
@endsection