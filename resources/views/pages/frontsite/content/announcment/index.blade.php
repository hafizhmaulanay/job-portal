@extends('layouts.default')

@section('title', 'Pengumuman | Penara')

@section('content')
<main id="content" role="main" class="">
    <!-- Content -->
        <!-- Card Grid -->
        <div class="container content-space-t-1 content-space-t-lg-2 content-space-b-2 content-space-b-lg-3">
            <!-- Heading -->
            <div class="w-md-75 w-lg-50 text-center mx-md-auto mb-7">
                <h2>Pengumuman Seleksi Lowongan Kerja</h2>
            </div>
            <!-- End Heading -->

            <div class="row row-cols-1 mb-5">

                @forelse ($announcement as $announcement_item)
                    <div class="col mb-3 mb-sm-4">
                        <!-- Card -->
                        <a class="card card-sm card-bordered card-transition h-100" href="{{ route('announcement.show', $announcement_item->slug) }}">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h5 class="card-title text-inherit">{{ isset($announcement_item->title) ? Str::title($announcement_item->title) : '' }}</h5>
                                        <p class="card-text text-body small">{{ isset($announcement_item->start_date) ? $announcement_item->start_date->format('d F Y') : '' }} -  {{ isset($announcement_item->end_date) ? $announcement_item->end_date->format('d F Y') : '' }}</p>
                                    </div>
                                    <!-- End Col -->
                                    
                                    <div class="col-auto">
                                        <span class="text-muted">
                                            <i class="bi-chevron-right small"></i>
                                        </span>
                                    </div>
                                    <!-- End Col -->
                                </div>
                                <!-- End Row -->
                            </div>
                        </a>
                        <!-- End Card -->
                    </div>
                    <!-- End Col -->
                @empty
                    <!-- Content -->
                    <div class="container col my-5 text-center">
                        <div class="mb-3">
                        <img class="img-fluid" src="{{ asset('front-design/assets/img/illustrations/sorry.png') }}" alt="Image Description" style="width: 18rem;">
                        </div>

                        <div class="mb-4">
                        <h2 class="my-2">Oops!</h2>
                        <p class="fs-4">Pengumuman tidak ditemukan.</p>
                        </div>
                    </div>
                @endforelse
            </div>

            <!-- End Row -->
            
            <!-- Pagination -->
            @if ($announcement->hasPages())
                <div class="mb-10">
                    <div class="table-responsive">
                        <div class="text-center mb-3">
                            @if ($announcement->hasPages())
                                <h4 class="card-title">Pagination Page</h4>
                                <p class="mt-1"><code class="text-dark">Page
                                    {{ $announcement->currentPage() }} of {{ $announcement->lastPage() }}</code>
                                    All data <code class="text-dark">{{ $announcement->total() }}</code>
                                </p>
                            @endif
                            <nav aria-label="Page navigation">
                                @if ($announcement->hasPages())
                                    <ul class="pagination justify-content-center pagination-round">

                                        {{-- Previous Page Link --}}
                                        @if ($announcement->onFirstPage())
                                            <li class="page-item disabled">
                                                <a class="page-link" href="#"
                                                    aria-label="Previous">
                                                    <i class="bi-chevron-double-left small"></i>
                                                </a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                    href="{{ $announcement->previousPageUrl() }}"
                                                    aria-label="Previous">
                                                    <i class="bi-chevron-double-left small"></i>
                                                </a>
                                            </li>
                                        @endif

                                        @if ($announcement->currentPage() > 2)
                                            <li class="page-item"><a class="page-link"
                                                    href="{{ $announcement->url(1) }}">1</a></li>
                                        @endif

                                        @if ($announcement->currentPage() > 3)
                                            <li class="page-item disabled"><a class="page-link"
                                                    href="#">...</a></li>
                                        @endif

                                        @foreach (range(1, $announcement->lastPage()) as $i)
                                            @if ($i >= $announcement->currentPage() - 1 && $i <= $announcement->currentPage() + 1)
                                                @if ($i == $announcement->currentPage())
                                                    <li class="page-item active"><span
                                                            class="page-link">{{ $i }}</span>
                                                    </li>
                                                @else
                                                    <li class="page-item"><a class="page-link"
                                                            href="{{ $announcement->url($i) }}">{{ $i }}</a>
                                                    </li>
                                                @endif
                                            @endif
                                        @endforeach

                                        @if ($announcement->currentPage() < $announcement->lastPage() - 3)
                                            <li class="page-item disabled"><a class="page-link"
                                                    href="#">...</a></li>
                                        @endif

                                        @if ($announcement->currentPage() < $announcement->lastPage() - 2)
                                            <li class="page-item"><a class="page-link"
                                                    href="{{ $announcement->url($announcement->lastPage()) }}">{{ $announcement->lastPage() }}</a>
                                            </li>
                                        @endif

                                        {{-- Next Page Link --}}
                                        @if ($announcement->hasMorePages())
                                            <li class="page-item">
                                                <a class="page-link"
                                                    href="{{ $announcement->nextPageUrl() }}"
                                                    aria-label="Next">
                                                    <i class="bi-chevron-double-right small"></i>
                                                </a>
                                            </li>
                                        @else
                                            <li class="page-item disabled">
                                                <a class="page-link" href="#" aria-label="Next">
                                                    <i class="bi-chevron-double-right small"></i>
                                                </a>
                                            </li>
                                        @endif

                                    </ul>
                                @endif
                            </nav>
                        </div>
                    </div>
                </div>
            @endif
            <!-- End Pagination -->

        </div>
        <!-- End Card Grid -->
    <!-- End Content -->
</main>
@endsection
