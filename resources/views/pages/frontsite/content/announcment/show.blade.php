@extends('layouts.default')

@section('title', 'Detail Pengumuman | Penara')

@section('content')
<main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">
                                {{ $announcement->title ?? '' }}
                            </h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Pengumuman</li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    {{ $announcement->title ?? '' }}
                                </li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <a class="btn btn-soft-light btn-sm" href="{{ route('announcement.index') }}">
                            <i class="fas fa-arrow-left me-1"></i>
                            Kembali
                        </a>
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">

            <div class="row">

                <div class="col-lg-12">
                    <!-- Card -->
                    <div class="card">
                        <!-- Body -->
                        <div class="card-body">

                             <!-- Accordion -->
                            <div class="accordion accordion-btn-icon-start mb-4" id="accordionExample">
                                <div class="accordion-item">
                                    <div class="accordion-header" id="headingOne">
                                        <a class="accordion-button collapsed" role="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span class="ps-2">Catatan Pengumuman</span>
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                        {{ $announcement->description ?? '' }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Table -->
                            <div class="table-responsive pb-5">
                                <table id="example" class="table table-thead-bordered table-nowrap table-align-middle pt-3">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Nama Perusahaan</th>
                                            <th>Posisi</th>
                                            <th>Tahap</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                    
                                    <tbody>

                                        @forelse ($applicant as $applicant_item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    {{ $applicant_item->user->name ?? '' }}
                                                </td>
                                                <td>
                                                    {{ $applicant_item->user->email ?? '' }}
                                                </td>
                                                <td>
                                                    {{ $applicant_item->job_vacancy->company->user->name ?? '' }}
                                                </td>
                                                <td>
                                                    {{ $applicant_item->job_vacancy->position->name ?? '' }}
                                                </td>
                                                <td>
                                                    @if ($applicant_item->step == 1)
                                                        <span class="badge bg-secondary rounded-pill">
                                                            Menunggu Direview
                                                        </span>
                                                    @elseif ($applicant_item->step == 2)
                                                        <span class="badge bg-warning rounded-pill">
                                                            Direview
                                                        </span>
                                                    @elseif ($applicant_item->step == 3)
                                                        <span class="badge bg-primary rounded-pill">
                                                            Tes Tertulis
                                                        </span>
                                                    @elseif ($applicant_item->step == 4)
                                                        <span class="badge bg-primary rounded-pill">
                                                            Interview
                                                        </span>
                                                    @elseif ($applicant_item->step == 5)
                                                        <span class="badge bg-info rounded-pill">
                                                            Penawaran
                                                        </span>
                                                    @elseif ($applicant_item->step == 6)
                                                        <span class="badge bg-danger rounded-pill">
                                                            Ditolak
                                                        </span>
                                                    @endif
                                                </td>
                                                 {{-- Result (test, interview, offer) --}}
                                                @if ($applicant_item->step == 3)
                                                    <td>
                                                        @if ($applicant_item->result_test == 1)
                                                            <span class="badge bg-success rounded-pill">
                                                                Lolos
                                                            </span>
                                                        @elseif ($applicant_item->result_test == 2)
                                                            <span class="badge bg-danger rounded-pill">
                                                                Tidak Lolos
                                                            </span>
                                                        @else
                                                            {{ 'N/A' }}
                                                        @endif
                                                    </td>
                                                @elseif ($applicant_item->step == 4)
                                                    <td>
                                                        @if ($applicant_item->result_interview == 1)
                                                            <span class="badge bg-success rounded-pill">
                                                                Lolos
                                                            </span>
                                                        @elseif ($applicant_item->result_interview == 2)
                                                            <span class="badge bg-danger rounded-pill">
                                                                Tidak Lolos
                                                            </span>
                                                        @else
                                                            {{ 'N/A' }}
                                                        @endif
                                                    </td>
                                                @elseif ($applicant_item->step == 5)
                                                    <td>
                                                        @if ($applicant_item->result_offer == 1)
                                                            <span class="badge bg-success rounded-pill">
                                                                Lolos
                                                            </span>
                                                        @elseif ($applicant_item->result_offer == 2)
                                                            <span class="badge bg-danger rounded-pill">
                                                                Tidak Lolos
                                                            </span>
                                                        @else
                                                            {{ 'N/A' }}
                                                        @endif
                                                    </td>
                                                @else
                                                    <td>
                                                        {{ 'N/A' }}
                                                    </td>
                                                @endif
                                            </tr>
                                        @empty
                                            {{-- Not found --}}
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- End Table -->

                            <hr>

                            <div class="table-responsive">
                                <div class="text-center mb-3">
                                    @if ($applicant->hasPages())
                                        <h4 class="card-title">Pagination Page</h4>
                                        <p class="mt-1"><code class="text-dark">Page
                                            {{ $applicant->currentPage() }} of {{ $applicant->lastPage() }}</code>
                                            All data <code class="text-dark">{{ $applicant->total() }}</code>
                                        </p>
                                    @endif
                                    <nav aria-label="Page navigation">
                                        @if ($applicant->hasPages())
                                            <ul class="pagination justify-content-center pagination-round">

                                                {{-- Previous Page Link --}}
                                                @if ($applicant->onFirstPage())
                                                    <li class="page-item disabled">
                                                        <a class="page-link" href="#"
                                                            aria-label="Previous">
                                                            <i class="bi-chevron-double-left small"></i>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href="{{ $applicant->previousPageUrl() }}"
                                                            aria-label="Previous">
                                                            <i class="bi-chevron-double-left small"></i>
                                                        </a>
                                                    </li>
                                                @endif

                                                @if ($applicant->currentPage() > 2)
                                                    <li class="page-item"><a class="page-link"
                                                            href="{{ $applicant->url(1) }}">1</a></li>
                                                @endif

                                                @if ($applicant->currentPage() > 3)
                                                    <li class="page-item disabled"><a class="page-link"
                                                            href="#">...</a></li>
                                                @endif

                                                @foreach (range(1, $applicant->lastPage()) as $i)
                                                    @if ($i >= $applicant->currentPage() - 1 && $i <= $applicant->currentPage() + 1)
                                                        @if ($i == $applicant->currentPage())
                                                            <li class="page-item active"><span
                                                                    class="page-link">{{ $i }}</span>
                                                            </li>
                                                        @else
                                                            <li class="page-item"><a class="page-link"
                                                                    href="{{ $applicant->url($i) }}">{{ $i }}</a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endforeach

                                                @if ($applicant->currentPage() < $applicant->lastPage() - 3)
                                                    <li class="page-item disabled"><a class="page-link"
                                                            href="#">...</a></li>
                                                @endif

                                                @if ($applicant->currentPage() < $applicant->lastPage() - 2)
                                                    <li class="page-item"><a class="page-link"
                                                            href="{{ $applicant->url($applicant->lastPage()) }}">{{ $applicant->lastPage() }}</a>
                                                    </li>
                                                @endif

                                                {{-- Next Page Link --}}
                                                @if ($applicant->hasMorePages())
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href="{{ $applicant->nextPageUrl() }}"
                                                            aria-label="Next">
                                                            <i class="bi-chevron-double-right small"></i>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li class="page-item disabled">
                                                        <a class="page-link" href="#" aria-label="Next">
                                                            <i class="bi-chevron-double-right small"></i>
                                                        </a>
                                                    </li>
                                                @endif

                                            </ul>
                                        @endif
                                    </nav>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection

@push('after-style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
@endpush

@push('after-script')
    <script src="{{ asset('back-design/app-assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    
@endpush