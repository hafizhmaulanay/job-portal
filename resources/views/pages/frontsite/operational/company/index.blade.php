@extends('layouts.default')

{{-- Set title --}}
@section('title', 'Daftar Perusahaan | Penara')

{{-- Set page content --}}
    
@section('content')
<!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main" class="pb-10">
    <!-- Form -->
    <div class="container content-space-1 content-space-lg-2">
      <div class="w-lg-75 mx-lg-auto">
        <!-- Heading -->
        <div class="text-center mb-2">
          <h1 class="h2">Daftar Perusahaan</h1>
          <p class="mb-0">Lengkapi form di bawah dengan menggunakan data Anda yang valid</p>
        </div>
        <!-- End Heading -->

        <!-- Error -->
        @if ($errors->any())
            <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                <ul class="mt-3 mb-0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <!-- End Error -->

        <!-- Success -->
         @if (session()->has('success'))
            <div class="alert alert-soft-success alert-dismissible fade show mt-4 text-center" role="alert">
                <div class="fw-semi-bold">
                  Success! Perusahan anda sedang di review oleh penara.
                  {{ session()->get('success') }}
                </div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        {{-- end Success --}}
        

        <form action="{{ route('company.store') }}" method="POST">
          
          @csrf

          <div class="mb-5">

            <div class="row mt-7">
              <div class="col-sm-6">
                 <!-- Form -->
                <div class="mb-3">
                  <label class="form-label" for="pic_name">Nama PIC</label>
                  <input type="text" class="form-control form-control-lg" name="pic_name" id="pic_name" value="{{ old('pic_name') }}" autocomplete="off" placeholder="Nama PIC" required autofocus>
                  
                  @if($errors->has('pic_name'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('pic_name') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                  <label class="form-label" for="name">Nama Perusahaan</label>
                  <input type="text" class="form-control form-control-lg" id="name" name="name" value="{{ old('name') }}" autocomplete="off" placeholder="Nama Perusahaan" required>
                  
                  @if($errors->has('name'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('name') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->

            <div class="row">
              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                  <label class="form-label" for="email">Email</label>
                  <input type="email" class="form-control form-control-lg" name="email" id="email" value="{{ old('email') }}" autocomplete="off" placeholder="people@gmail.com" required>
                
                  @if($errors->has('email'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-4">
                  <label class="form-label" for="contact">No Handphone</label>
                  <input type="text" class="js-input-mask form-control form-control-lg" name="contact" id="contact" value="{{ old('contact') }}" autocomplete="off" placeholder="+62 xxx xxxx xxxx" data-hs-mask-options='{
                      "mask": "+62 000 0000 0000"
                  }' required>
                  
                  @if($errors->has('contact'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('contact') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->

            <div class="row">

              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                    <div class="d-flex justify-content-between align-items-center">
                        <label class="form-label" for="password">Password</label>
                    </div>

                    <div class="input-group input-group-merge">
                        <input type="password" class="js-toggle-password form-control form-control-lg" name="password" id="password" autocomplete="new-password" placeholder="Password" aria-label="Password" required
                                data-hs-toggle-password-options='{
                                "target": "#changePassTargetPassword",
                                "defaultClass": "bi-eye-slash",
                                "showClass": "bi-eye",
                                "classChangeTarget": "#changePassIconPassword"
                            }'>
                        <a id="changePassTargetPassword" class="input-group-append input-group-text" href="javascript:;">
                            <i id="changePassIconPassword" class="bi-eye"></i>
                        </a>  
                      </div>

                      @if($errors->has('password'))
                          <span style="font-style: bold; color: red;">{{ $errors->first('password') }}</span>
                      @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->
              

              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                    <div class="d-flex justify-content-between align-items-center">
                        <label class="form-label" for="password_confirmation">Konfirmasi Password</label>
                    </div>

                    <div class="input-group input-group-merge">
                        <input type="password" class="js-toggle-password form-control form-control-lg" name="password_confirmation" id="password_confirmation" autocomplete="new-password" placeholder="Konfirmasi Password" aria-label="Konfirmasi Password" required
                                data-hs-toggle-password-options='{
                                "target": "#changePassTargetPasswordConfirmation",
                                "defaultClass": "bi-eye-slash",
                                "showClass": "bi-eye",
                                "classChangeTarget": "#changePassIconPasswordConfirmation"
                            }'>
                        <a id="changePassTargetPasswordConfirmation" class="input-group-append input-group-text" href="javascript:;">
                            <i id="changePassIconPasswordConfirmation" class="bi-eye"></i>
                        </a>
                        
                    </div>
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->
          </div>

          <div class="mt-4">
            <div class="d-grid">
              <button type="submit" class="btn btn-gold btn-lg">Daftar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
    <!-- End Form -->
  </main>
<!-- ========== END MAIN CONTENT ========== -->
@endsection