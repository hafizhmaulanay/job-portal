@extends('layouts.default')

@section('title', 'Job Detail | Penara')

@section('content')
     <!-- Page Header -->
    <div class="container pt-4">
      <div class="page-header">

        <!-- Media -->
        <div class="d-sm-flex align-items-lg-center pt-5 px-3 pb-3">
          <div class="flex-shrink-0 mb-2 mb-sm-0 me-4">
             <img src="
                @if ($job_vacancy->company->image != "")
                    {{ $job_vacancy->company->image_url }}
                @else
                    {{ asset('/back-design/clients/default/image-not-found.svg') }}
                @endif "
                alt="{{ $job_vacancy->company->name ?? 'job vacancy logo' }}" class="avatar avatar-xl profile-cover-avatar shadow-sm">
          </div>

          @php
              $company_is_active = $job_vacancy->company->status == 1;
              $job_vacancy_is_active = $job_vacancy->status == 1;
              $job_vacancy_is_valid = $job_vacancy->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('end_date', '>=', date('Y-m-d'))->count() > 0;
              $job_vacancy_contract_is_valid = $job_vacancy->company()->whereHas('company_contract_history', function($query) {
                  $query->whereHas('company_contract', function($query) {
                      $query->where('status', 1)->whereDate('end_period', '>=', date('Y-m-d'));
                  });
              })->count() > 0;
          @endphp
          
          <div class="flex-grow-1">
            <div class="row">
              <div class="col-md mb-3 mb-lg-0">
                <h1 class="h2 mb-1">{{ $job_vacancy->position->name ?? '' }}</h1>
                {{-- badge closed --}}
                @unless ($company_is_active && $job_vacancy_is_active && $job_vacancy_is_valid && $job_vacancy_contract_is_valid)
                    <div class="btn btn-sm bg-soft-dark" style="cursor: default">
                        Closed
                    </div>
                @endunless

                <div class="d-flex gap-1">
                  <span class="">{{ $job_vacancy->company->name ?? '' }}</span>
                </div>

                <ul class="list-inline list-separator small text-body mt-2">
                  <li class="list-inline-item">Posted {{ $job_vacancy->created_at->diffForHumans() }}</li>
                  <li class="list-inline-item">{{ $job_vacancy->provinces->name }}</li>
                </ul>
                
              </div>
              <!-- End Col -->
              
              <div class="col-md-auto align-self-md-end">
                <div class="d-grid d-sm-flex gap-2">

                  @if ($company_is_active && $job_vacancy_is_active && $job_vacancy_is_valid && $job_vacancy_contract_is_valid)
                    @auth

                      @php
                          $get_latest_apply = Auth::user()->applicant()->where('job_vacancy_id', $job_vacancy->id)->latest()->first();

                          $test_failed = $get_latest_apply?->step == 3 && $get_latest_apply?->result_test == 2;
                          $interview_failed = $get_latest_apply?->step == 4 && $get_latest_apply?->result_interview == 2;
                          $offer = $get_latest_apply?->step == 5 && $get_latest_apply?->result_offer == 1;
                          $offer_failed = $get_latest_apply?->step == 5 && $get_latest_apply?->result_offer == 2;
                          $rejected = $get_latest_apply?->step == 6
                      @endphp

                      @if (!Auth::user()->applicant->contains('job_vacancy_id', $job_vacancy->id) || $test_failed || $interview_failed || $offer || $offer_failed || $rejected)
                          <form action="{{ route('applicant.store') }}" method="POST">
                            
                            @csrf

                            <input type="hidden" name="job_vacancy_id" value="{{ $job_vacancy->id }}">
                            <button type="submit" class="btn btn-gold w-100">Lamar Sekarang</button>
                          </form>
                      @endif
                      @else
                          <a href="{{ route('login') }}" class="btn btn-gold">Lamar Sekarang</a>
                    @endauth
                  @endif
                  
                  @auth
                      @if (Auth::user()->job_wishlist->contains($job_vacancy->id))
                        <form action="{{ route('job_wishlist.destroy', $job_vacancy->id) }}" method="POST"
                            onsubmit="return confirm('Apakah Anda yakin ingin dihapus dari wishlist Anda ?');">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-outline-gold w-100">
                                <i class="bi-trash me-1"></i> Hapus dari Wishlist
                            </button>
                        </form>
                      @else
                          @if ($company_is_active && $job_vacancy_is_active && $job_vacancy_is_valid && $job_vacancy_contract_is_valid)
                            <form action="{{ route('job_wishlist.store') }}" method="POST">
                              
                              @csrf

                              <input type="hidden" name="job_vacancy_id" value="{{ $job_vacancy->id }}">
                              <button class="btn btn-outline-gold w-100" type="submit"><i class="bi bi-save me-1"></i> Simpan</button>
                            </form>
                          @endif
                      @endif
                  @else
                      @if ($company_is_active && $job_vacancy_is_active && $job_vacancy_is_valid && $job_vacancy_contract_is_valid)
                          <a href="{{ route('login') }}" class="btn btn-outline-gold"><i class="bi bi-save me-1"></i> Simpan</a>
                      @endif
                  @endauth

                  @if ($company_is_active && $job_vacancy_is_active && $job_vacancy_is_valid && $job_vacancy_contract_is_valid)
                    <button type="button" id="btnShare" data-bs-html="true" data-clipboard-text="{{ url()->current() }}" data-bs-toggle="tooltip" title="Copy to clipboard" class="btn btn-outline-gold">
                      <i class="bi bi-share me-1"></i> Bagikan
                    </button>
                  @endif
                </div>
              </div>
              <!-- End Col -->
            </div>
            <!-- End Row -->
          </div>
        </div>
        <!-- End Media -->
      </div>
    </div>
    <!-- End Page Header -->

    <!-- Content -->
    <div id="about-section" class="container content-space-t-lg-1 pt-3 content-space-b-2">
      <h3 class="mb-4">Deskripsi Pekerjaan</h3>

      <div class="row mb-5">
        <div class="col-md-3 order-2 mb-3 mb-md-0">
          <div class="ps-md-4">
            <!-- List -->
            <ul class="list-unstyled list-py-2">
              <li>
                <h4>Tunjangan:</h4>

                <ul class="list-checked list-checked-success">

                  @forelse ($job_vacancy->benefit as $benefit)
                    <li class="list-checked-item">{{ $benefit->name }}</li>
                  @empty
                    <li class="list-checked-item">Tidak ada Tunjangan</li>
                  @endforelse

                </ul>
              </li>
            </ul>
            <!-- End List -->
          </div>
        </div>
        <!-- End Col -->

        <div class="col-md-9">
          {!! $job_vacancy->description ?? '' !!}
        </div>
        <!-- End Col -->
      </div>
      <!-- End Row -->

      <h3 class="mb-4">Informasi Tambahan</h3>

     <div class="row">
            <div class="col-sm-6">
              <div class="mb-4">
                <h5>Kualifikasi Pendidikan</h5>
                <p>{{ $job_vacancy->level_of_education->name ?? '' }}</p>
              </div>
              
              <div class="mb-4">
                <h5>Program Studi</h5>
                <p>{{ $job_vacancy->learning_program->name ?? '' }}</p>
              </div>
            </div>
            <!-- End Col -->

            <div class="col-sm-6">
              <div class="mb-4">
                <h5>Pengalaman Kerja</h5>

                @isset($job_vacancy->experience_year)
                  <p>{{ $job_vacancy->experience_year ?? '' }} tahun</p>
                @else
                  <p>Tidak ada batasan</p>
                @endisset

              </div>

              <div class="mb-4">
                <h5>Tipe Pekerjaan</h5>
                <p>{{ $job_vacancy->job_type->name ?? '' }}</p>
              </div>
            </div>
            <!-- End Col -->
      </div>
      <!-- End Row -->

    </div>
    <!-- Content -->
@endsection

@push('after-script')
    <script src="https://unpkg.com/clipboard@2/dist/clipboard.min.js"></script>

    <script>
      let clipboard = new ClipboardJS('#btnShare');
      
      clipboard.on('success', function (e) {

          Toastify({
            // icon check
            text: "Link berhasil disalin!",
            style: {
              background: "#00C9A7",
              color: "#ffffff",
              fontSize: "15px",
              textAlign: "center"
            },
            posisition: "top-center",
          }).showToast();

      });
    </script>
@endpush