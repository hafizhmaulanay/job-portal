@extends('layouts.default')

{{-- set section --}}
@section('title', 'Lamaran Saya | Penara')

@section('content')
    <main id="content" role="main" class="">
        <!-- Breadcrumb -->
        <div class="navbar-dark bg-gold" style="background-image: url({{ asset('front-design/assets/svg/components/wave-pattern-light.svg') }});">
            <div class="container content-space-1 content-space-b-lg-3">
                <div class="row align-items-center">
                    <div class="col">
                        <div class="d-none d-lg-block">
                            <h1 class="h2 text-white">Lamaran Saya</h1>
                        </div>

                        <!-- Breadcrumb -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-light mb-0">
                                <li class="breadcrumb-item">Lamaran</li>
                                <li class="breadcrumb-item active" aria-current="page">Lamaran Saya</li>
                            </ol>
                        </nav>
                        <!-- End Breadcrumb -->
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                        <div class="d-none d-lg-block">
                            <a class="btn btn-soft-light btn-sm" href="{{ route('home') }}">Kembali ke home</a>
                        </div>
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Breadcrumb -->

        <!-- Content -->
        <div class="container content-space-1 content-space-t-lg-0 content-space-b-lg-2 mt-lg-n10">

            <div class="row">

                <div class="col-lg-12">
                    <!-- Card -->
                    <div class="card">

                        <!-- Filter -->
                        <div class="card-header pb-0">
                            <div>
                                <div class="row justify-content-md-start align-items-md-center">

                                    <div class="col-md-12">
                                        <div class="row gx-2 align-items-center">
                                            <div class="col-md-8 text-start mb-2">
                                               <h4>
                                                   Filter Tahap :
                                               </h4>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col mb-2">
                                                <form action="{{ route('applicant.filter') }}" method="GET">
                                                    <!-- Select -->
                                                    <div class="tom-select-custom">
                                                        <select class="js-select form-select" autocomplete="off"
                                                                name="step" onchange="this.form.submit()"
                                                                data-hs-tom-select-options='{
                                                                    "searchInDropdown": false,
                                                                    "hidePlaceholderOnSearch": true,
                                                                    "placeholder": "Tahap"
                                                                }'>
                                                            <option value="">Tahap</option>
                                                            <option value="1" {{ request('step') == 1 ? 'selected' : '' }}>
                                                                Menunggu Direview
                                                            </option>
                                                            <option value="2" {{ request('step') == 2 ? 'selected' : '' }}>
                                                                Review
                                                            </option>
                                                            <option value="3" {{ request('step') == 3 ? 'selected' : '' }}>
                                                                Tes Tulis
                                                            </option>
                                                            <option value="4" {{ request('step') == 4 ? 'selected' : '' }}>
                                                                Interview
                                                            </option>
                                                            <option value="5" {{ request('step') == 5 ? 'selected' : '' }}>
                                                                Penawaran
                                                            </option>
                                                            <option value="6" {{ request('step') == 6 ? 'selected' : '' }}>
                                                                Ditolak
                                                            </option>
                                                            <option value="7" {{ request('step') == 7 ? 'selected' : '' }}>
                                                                Blacklist
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <!-- End Select -->
                                                </form>
                                            </div>
                                            <!-- End Col -->

                                            {{-- reset --}}
                                            @if(request('step'))
                                                <div class="col-auto mb-2">
                                                    <a href="{{ route('applicant.my_application') }}" class="btn btn-outline-seconday">
                                                        Reset
                                                    </a>   
                                                </div>
                                            @endif
                                        </div>
                                        <!-- End Row -->
                                    </div>
                                    <!-- End Col -->
                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Filter -->

                        <!-- Body -->
                        <div class="card-body">
                           <!-- Table -->
                            <div class="table-responsive pb-5">
                                <table id="default-table" class="table table-thead-bordered table-align-middle pt-3">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Perusahaan</th>
                                            <th>Posisi</th>
                                            <th>Tanggal Lamar</th>
                                            <th>Tahap</th>
                                            <th>Hasil</th>
                                            <th style="text-align: center; width: 150px">Aksi</th>
                                        </tr>
                                    </thead>
                    
                                    <tbody>

                                        @forelse ($my_application as $my_application_item)
                                            <tr>
                                                <td>{{ $loop->iteration }}.</td>
                                                <td>{{ $my_application_item->job_vacancy->company->user->name ?? '' }}</td>
                                                <td>{{ $my_application_item->job_vacancy->position->name ?? '' }}</td>
                                                <td>{{ isset($my_application_item->created_at) ? $my_application_item->created_at->format('d, F Y') : '' }}</td>
                                                <td>
                                                    @if ($my_application_item->step == 1)
                                                        <span class="badge bg-secondary rounded-pill">
                                                            Menunggu Direview
                                                        </span>
                                                    @elseif ($my_application_item->step == 2)
                                                        <span class="badge bg-warning rounded-pill">
                                                            Review
                                                        </span>
                                                    @elseif ($my_application_item->step == 3)
                                                        <span class="badge bg-primary rounded-pill">
                                                            Tes Tertulis
                                                        </span>
                                                    @elseif ($my_application_item->step == 4)
                                                        <span class="badge bg-primary rounded-pill">
                                                            Interview
                                                        </span>
                                                    @elseif ($my_application_item->step == 5)
                                                        <span class="badge bg-info rounded-pill">
                                                            Penawaran
                                                        </span>
                                                    @elseif ($my_application_item->step == 6)
                                                        <span class="badge bg-danger rounded-pill">
                                                            Ditolak
                                                        </span>
                                                    @elseif ($my_application_item->step == 7)
                                                        <span class="badge bg-danger rounded-pill">
                                                            <span>Blacklist</span>
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($my_application_item->step == 3 && $my_application_item->result_test == 1)
                                                        <span class="badge bg-success rounded-pill">
                                                            Lolos
                                                        </span>
                                                    @elseif ($my_application_item->step == 3 && $my_application_item->result_test == 2)
                                                        <span class="badge bg-danger rounded-pill">
                                                            Tidak Lolos
                                                        </span>
                                                    @elseif ($my_application_item->step == 4 && $my_application_item->result_interview == 1)
                                                        <span class="badge bg-success rounded-pill">
                                                            Lolos
                                                        </span>
                                                    @elseif ($my_application_item->step == 4 && $my_application_item->result_interview == 2)
                                                        <span class="badge bg-danger rounded-pill">
                                                            Tidak Lolos
                                                        </span>
                                                    @elseif ($my_application_item->step == 5 && $my_application_item->result_offer == 1)
                                                        <span class="badge bg-success rounded-pill">
                                                            Lolos
                                                        </span>
                                                    @elseif ($my_application_item->step == 5 && $my_application_item->result_offer == 2)
                                                        <span class="badge bg-danger rounded-pill">
                                                            Tidak Lolos
                                                        </span>
                                                    @else
                                                        {{ 'N/A' }}
                                                    @endif
                                                </td>
                                                <td style="text-align: center; width: 150px">
                                                    <!-- Dropdown -->
                                                    <div class="btn-group">
                                                        <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButtonInfo" data-bs-toggle="dropdown" aria-expanded="false">
                                                            Aksi
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonInfo">
                                                            <a class="dropdown-item" href="#showModal" data-bs-toggle="modal" 
                                                                data-bs-target="#showModal" data-remote="{{ route('applicant.show', $my_application_item->id) }}" data-title="Lamaran Detail">
                                                                Lihat
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!-- End Dropdown -->
                                                </td>
                                            </tr>
                                        @empty
                                            {{-- Not found --}}
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- End Table -->

                            <hr>

                            <div class="table-responsive">
                                <div class="text-center mb-3">
                                    @if ($my_application->hasPages())
                                        <h4 class="card-title">Pagination Page</h4>
                                        <p class="mt-1"><code class="text-dark">Page
                                            {{ $my_application->currentPage() }} of {{ $my_application->lastPage() }}</code>
                                            All data <code class="text-dark">{{ $my_application->total() }}</code>
                                        </p>
                                    @endif
                                    <nav aria-label="Page navigation">
                                        @if ($my_application->hasPages())
                                            <ul class="pagination justify-content-center pagination-round">

                                                {{-- Previous Page Link --}}
                                                @if ($my_application->onFirstPage())
                                                    <li class="page-item disabled">
                                                        <a class="page-link" href="#"
                                                            aria-label="Previous">
                                                            <i class="bi-chevron-double-left small"></i>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href="{{ $my_application->previousPageUrl() }}"
                                                            aria-label="Previous">
                                                            <i class="bi-chevron-double-left small"></i>
                                                        </a>
                                                    </li>
                                                @endif

                                                @if ($my_application->currentPage() > 2)
                                                    <li class="page-item"><a class="page-link"
                                                            href="{{ $my_application->url(1) }}">1</a></li>
                                                @endif

                                                @if ($my_application->currentPage() > 3)
                                                    <li class="page-item disabled"><a class="page-link"
                                                            href="#">...</a></li>
                                                @endif

                                                @foreach (range(1, $my_application->lastPage()) as $i)
                                                    @if ($i >= $my_application->currentPage() - 1 && $i <= $my_application->currentPage() + 1)
                                                        @if ($i == $my_application->currentPage())
                                                            <li class="page-item active"><span
                                                                    class="page-link">{{ $i }}</span>
                                                            </li>
                                                        @else
                                                            <li class="page-item"><a class="page-link"
                                                                    href="{{ $my_application->url($i) }}">{{ $i }}</a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endforeach

                                                @if ($my_application->currentPage() < $my_application->lastPage() - 3)
                                                    <li class="page-item disabled"><a class="page-link"
                                                            href="#">...</a></li>
                                                @endif

                                                @if ($my_application->currentPage() < $my_application->lastPage() - 2)
                                                    <li class="page-item"><a class="page-link"
                                                            href="{{ $my_application->url($my_application->lastPage()) }}">{{ $my_application->lastPage() }}</a>
                                                    </li>
                                                @endif

                                                {{-- Next Page Link --}}
                                                @if ($my_application->hasMorePages())
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href="{{ $my_application->nextPageUrl() }}"
                                                            aria-label="Next">
                                                            <i class="bi-chevron-double-right small"></i>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li class="page-item disabled">
                                                        <a class="page-link" href="#" aria-label="Next">
                                                            <i class="bi-chevron-double-right small"></i>
                                                        </a>
                                                    </li>
                                                @endif

                                            </ul>
                                        @endif
                                    </nav>
                                </div>
                            </div>

                        </div>
                        <!-- End Body -->
                    </div>
                    <!-- End Card -->
                </div>
                <!-- End Col -->
            </div>
        </div>
    </main>
            
@endsection

@push('after-style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@push('after-script')
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
    
    <script>
        jQuery(document).ready(function($) {
            $('#showModal').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget);
                var modal = $(this);

                modal.find('.modal-body').load(button.data("remote"));
                modal.find('.modal-title').html(button.data("title"));
            });
        });

        $('#default-table').DataTable({
            "order": [],
            "paging": true,
            "lengthMenu": [
            [5, 10, 25, 50, 100, -1],
            [5, 10, 25, 50, 100, "All"]
            ],
            "pageLength": 10
        });
	</script>
    

    <!-- Modal -->
    <div id="showModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="showModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="showModalTitle">Detail Lamaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="spinner-border text-primary" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->
    
@endpush