<!-- Table -->
<table class="table table-vertical-border-striped">
    <tbody class="">
        <tr>
            <th>Perusahaan :</th>
            <td>{{ isset($my_application->job_vacancy->company->user->name) ? $my_application->job_vacancy->company->user->name : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Posisi :</th>
            <td>{{ isset($my_application->job_vacancy->position->name) ? $my_application->job_vacancy->position->name : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Tanggal Lamar :</th>
            <td>{{ isset($my_application->created_at) ? $my_application->created_at->format('d, F Y') : 'N/A' }}</td>
        </tr>
        <tr>
            <th>Tahap :</th>
            <td>
                @if ($my_application->step == 1)
                    <span class="badge bg-secondary rounded-pill">
                        Menunggu Direview
                    </span>
                @elseif ($my_application->step == 2)
                    <span class="badge bg-warning rounded-pill">
                        Review
                    </span>
                @elseif ($my_application->step == 3)
                    <span class="badge bg-primary rounded-pill">
                        Tes Tertulis
                    </span>
                @elseif ($my_application->step == 4)
                    <span class="badge bg-primary rounded-pill">
                        Interview
                    </span>
                @elseif ($my_application->step == 5)
                    <span class="badge bg-info rounded-pill">
                        Penawaran
                    </span>
                @elseif ($my_application->step == 6)
                    <span class="badge bg-danger rounded-pill">
                        Ditolak
                    </span>
                @elseif ($my_application->step == 7)
                    <span class="badge bg-danger rounded-pill">
                        Blacklist
                    </span>
                @endif
            </td>
        </tr>
        {{-- Tanggal (test, interview, offer) --}}
        @if ($my_application->step == 3)
            <tr>
                <th>Tanggal Test :</th>
                <td>{{ isset($my_application->schedule_test_date) ? $my_application->schedule_test_date->format('d, F Y H:i:s') : 'N/A' }}</td>
            </tr>
        @elseif ($my_application->step == 4)
            <tr>
                <th>Tanggal Interview :</th>
                <td>{{ isset($my_application->schedule_interview_date) ? $my_application->schedule_interview_date->format('d, F Y H:i:s') : 'N/A' }}</td>
            </tr>
        @elseif ($my_application->step == 5)
            <tr>
                <th>Tanggal Penawaran :</th>
                <td>{{ isset($my_application->schedule_offer_date) ? $my_application->schedule_offer_date->format('d, F Y H:i:s') : 'N/A' }}</td>
            </tr>
        @endif
        {{-- Present (test, interview, offer) --}}
        @if ($my_application->step == 3)
            <tr>
                <th>Kehadiran :</th>
                <td>
                    @if ($my_application->present_test == 1)
                        <span class="badge bg-success rounded-pill">
                            <span>Hadir</span>
                        </span>
                    @elseif ($my_application->present_test == 2)
                        <span class="badge bg-danger rounded-pill">
                            <span>Tidak Hadir</span>
                        </span>
                    @else
                        {{ 'N/A' }}
                    @endif
                </td>
            </tr>
        @elseif ($my_application->step == 4)
            <tr>
                <th>Kehadiran :</th>
                <td>
                    @if ($my_application->present_interview == 1)
                        <span class="badge bg-success rounded-pill">
                            <span>Hadir</span>
                        </span>
                    @elseif ($my_application->present_interview == 2)
                        <span class="badge bg-danger rounded-pill">
                            <span>Tidak Hadir</span>
                        </span>
                    @else
                        {{ 'N/A' }}
                    @endif
                </td>
            </tr>
        @elseif ($my_application->step == 5)
            <tr>
                <th>Kehadiran :</th>
                <td>
                    @if ($my_application->present_offer == 1)
                        <span class="badge bg-success rounded-pill">
                            <span>Hadir</span>
                        </span>
                    @elseif ($my_application->present_offer == 2)
                        <span class="badge bg-danger rounded-pill">
                            <span>Tidak Hadir</span>
                        </span>
                    @else
                        {{ 'N/A' }}
                    @endif
                </td>
            </tr>
        @endif
        {{-- Result (test, interview, offer) --}}
        @if ($my_application->step == 3)
            <tr>
                <th>Hasil :</th>
                <td>
                    @if ($my_application->result_test == 1)
                        <span class="badge bg-success rounded-pill">
                            <span>Lolos</span>
                        </span>
                    @elseif ($my_application->result_test == 2)
                        <span class="badge bg-danger rounded-pill">
                            <span>Tidak Lolos</span>
                        </span>
                    @else
                        {{ 'N/A' }}
                    @endif
                </td>
            </tr>
        @elseif ($my_application->step == 4)
            <tr>
                <th>Hasil :</th>
                <td>
                    @if ($my_application->result_interview == 1)
                        <span class="badge bg-success rounded-pill">
                            <span>Lolos</span>
                        </span>
                    @elseif ($my_application->result_interview == 2)
                        <span class="badge bg-danger rounded-pill">
                            <span>Tidak Lolos</span>
                        </span>
                    @else
                        {{ 'N/A' }}
                    @endif
                </td>
            </tr>
        @elseif ($my_application->step == 5)
            <tr>
                <th>Hasil :</th>
                <td>
                    @if ($my_application->result_offer == 1)
                        <span class="badge bg-success rounded-pill">
                            <span>Lolos</span>
                        </span>
                    @elseif ($my_application->result_offer == 2)
                        <span class="badge bg-danger rounded-pill">
                            <span>Tidak Lolos</span>
                        </span>
                    @else
                        {{ 'N/A' }}
                    @endif
                </td>
            </tr>
        @endif
        {{-- Note is result test is null (test, interview, offer) --}}
        @if ($my_application->result_test == null && $my_application->step == 3)
            <tr>
                <th>Catatan :</th>
                <td>{{ isset($my_application->note_test) ? $my_application->note_test : 'N/A' }}</td>
            </tr>
        @elseif ($my_application->result_interview == null && $my_application->step == 4)
            <tr>
                <th>Catatan :</th>
                <td>{{ isset($my_application->note_interview) ? $my_application->note_interview : 'N/A' }}</td>
            </tr>
        @elseif ($my_application->result_offer == null && $my_application->step == 5)
            <tr>
                <th>Catatan :</th>
                <td>{{ isset($my_application->note_offer) ? $my_application->note_offer : 'N/A' }}</td>
            </tr>
        @endif
        {{-- Note is result test is exist (test, interview, offer) --}}
        @if ($my_application->note_result_test != null && $my_application->step == 3)
            <tr>
                <th>Catatan :</th>
                <td>{{ isset($my_application->note_result_test) ? $my_application->note_result_test : 'N/A' }}</td>
            </tr>
        @elseif ($my_application->note_result_interview != null && $my_application->step == 4)
            <tr>
                <th>Catatan :</th>
                <td>{{ isset($my_application->note_result_interview) ? $my_application->note_result_interview : 'N/A' }}</td>
            </tr>
        @elseif ($my_application->note_result_offer != null && $my_application->step == 5)
            <tr>
                <th>Catatan :</th>
                <td>{{ isset($my_application->note_result_offer) ? $my_application->note_result_offer : 'N/A' }}</td>
            </tr>
        @endif
    </tbody>
</table>
<!-- End of Table -->