@extends('layouts.auth')

{{-- set title --}}
@section('title', 'Login')

@section('content')
    <!-- Form -->
    <div class="container content-space-3 content-space-lg-3">
        <div class="flex-grow-1 mx-auto" style="max-width: 28rem;">
            <!-- Heading -->
            <div class="text-center mb-5 mb-md-7">
                <a href="{{ route('home') }}">
                    <img class="w-100" style="max-width: 32rem" src="{{ asset('front-design/client-assets/images/penara-text-image.png') }}" alt="Logo">
                </a>
                <p>Masuk untuk menemukan karir yang tepat untuk Anda.</p>
            </div>
            <!-- End Heading -->

             <!-- Error -->
            @if ($errors->any())
                <div class="alert alert-soft-danger alert-dismissible fade show" style="margin-top: -20px; margin-bottom: 20px" role="alert">
                    <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                    <ul class="mt-3 mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <!-- End Error -->

            <!-- Form -->
            <form method="POST" action="{{ route('login') }}">

                @csrf

                <!-- Form -->
                <div class="mb-4">
                    <label class="form-label" for="email">Email</label>
                    <input type="email" class="form-control form-control-lg" value="{{ old('email') }}" name="email" id="email" placeholder="Email" aria-label="Email" autocomplete="off" autofocus required>
                    
                    @if($errors->has('email'))
                        <span style="font-style: bold; color: red;">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <!-- End Form -->

                <!-- Form -->
                <div class="mb-4">
                    <div class="d-flex justify-content-between align-items-center">
                        <label class="form-label" for="password">Password</label>
                        @if (Route::has('password.request'))
                            <a class="form-label-link" href="{{ route('password.request') }}">Lupa Password?</a>
                        @endif
                    </div>

                    <div class="input-group input-group-merge">
                        <input type="password" class="js-toggle-password form-control form-control-lg" name="password" id="password" autocomplete="off" placeholder="Password" aria-label="Password" required
                                data-hs-toggle-password-options='{
                                "target": "#changePassTarget",
                                "defaultClass": "bi-eye-slash",
                                "showClass": "bi-eye",
                                "classChangeTarget": "#changePassIcon"
                            }'>
                        <a id="changePassTarget" class="input-group-append input-group-text" href="javascript:;">
                            <i id="changePassIcon" class="bi-eye"></i>
                        </a>    
                    </div>

                    @if($errors->has('password'))
                        <span style="font-style: bold; color: red;">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <!-- End Form -->

                <div class="d-grid mb-3">
                    <button type="submit" class="btn btn-gold btn-lg">Masuk</button>
                </div>

                <div class="text-center">
                    <p>Belum punya akun? <a class="link" href="{{ route('register') }}">Daftar disini</a></p>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
    <!-- End Form -->
@endsection
