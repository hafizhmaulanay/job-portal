@extends('layouts.auth')

{{-- Set title --}}
@section('title', 'Register')
    
@section('content')
<!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <!-- Form -->
    <div class="container content-space-2">
      <div class="w-lg-75 mx-lg-auto">
        <!-- Heading -->
        <div class="text-center mb-2">
          <a href="{{ route('home') }}">
              <img class="w-100" style="max-width: 32rem" src="{{ asset('front-design/client-assets/images/penara-text-image.png') }}" alt="Logo">
          </a>
          {{-- <h1 class="h2">Selamat datang di Job Portal</h1> --}}
          <p class="mb-0">Lengkapi form di bawah dengan menggunakan data Anda yang valid.</p>
        </div>
        <!-- End Heading -->

        <!-- Error -->
        @if ($errors->any())
            <div class="alert alert-soft-danger alert-dismissible fade show mt-4" role="alert">
                <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                <ul class="mt-3 mb-0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <!-- End Error -->

        <form accept="{{ route('register') }}" method="POST">

          @csrf

          <div class="mb-5">

            <div class="row mt-7">
              <div class="col-sm-6">
                 <!-- Form -->
                <div class="mb-3">
                  <label class="form-label" for="name">Nama</label>
                  <input type="text" class="form-control form-control-lg" value="{{ old('name') }}" id="name" placeholder="Nama Lengkap" name="name" required autofocus autocomplete="name">

                  @if($errors->has('name'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('name') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                  <label class="form-label" for="number_id">NIK</label>
                  <input type="text" class="js-input-mask form-control form-control-lg" value="{{ old('number_id') }}" id="number_id" autocomplete="off" name="number_id" placeholder="NIK" data-hs-mask-options='{
                        "mask": "0000000000000000"
                    }' required>

                  @if($errors->has('number_id'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('number_id') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->

            <div class="row">
              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                  <label class="form-label" for="email">Email</label>
                  <input type="email" class="form-control form-control-lg" value="{{ old('email') }}" id="email" autocomplete="off" name="email" placeholder="people@gmail.com" required>
                  
                  @if($errors->has('email'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-4">
                  <label class="form-label" for="contact_number">No Handphone</label>
                  <input type="text" class="js-input-mask form-control form-control-lg" value="{{ old('contact_number') }}" id="contact_number" placeholder="+62 xxx xxxx xxxx" autocomplete="off" name="contact_number" data-hs-mask-options='{
                      "mask": "+62 000 0000 0000"
                  }' required>
                
                  @if($errors->has('contact_number'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('contact_number') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->

            <div class="row">

              <div class="col-sm-6">

                 <!-- Form -->
                <div class="mb-3">
                    <div class="d-flex justify-content-between align-items-center">
                        <label class="form-label" for="password">Password</label>
                    </div>

                    <div class="input-group input-group-merge">
                        <input type="password" class="js-toggle-password form-control form-control-lg" name="password" id="password" autocomplete="new-password" placeholder="Password" aria-label="Password" required
                                data-hs-toggle-password-options='{
                                "target": "#changePassTargetPassword",
                                "defaultClass": "bi-eye-slash",
                                "showClass": "bi-eye",
                                "classChangeTarget": "#changePassIconPassword"
                            }'>
                        <a id="changePassTargetPassword" class="input-group-append input-group-text" href="javascript:;">
                            <i id="changePassIconPassword" class="bi-eye"></i>
                        </a>  
                      </div>

                      @if($errors->has('password'))
                          <span style="font-style: bold; color: red;">{{ $errors->first('password') }}</span>
                      @endif
                </div>
                <!-- End Form -->

              </div>
              <!-- End Col -->
              

              <div class="col-sm-6">
                 <!-- Form -->
                <div class="mb-3">
                    <div class="d-flex justify-content-between align-items-center">
                        <label class="form-label" for="password_confirmation">Konfirmasi Password</label>
                    </div>

                    <div class="input-group input-group-merge">
                        <input type="password" class="js-toggle-password form-control form-control-lg" name="password_confirmation" id="password_confirmation" autocomplete="new-password" placeholder="Konfirmasi Password" aria-label="Konfirmasi Password" required
                                data-hs-toggle-password-options='{
                                "target": "#changePassTargetPasswordConfirmation",
                                "defaultClass": "bi-eye-slash",
                                "showClass": "bi-eye",
                                "classChangeTarget": "#changePassIconPasswordConfirmation"
                            }'>
                        <a id="changePassTargetPasswordConfirmation" class="input-group-append input-group-text" href="javascript:;">
                            <i id="changePassIconPasswordConfirmation" class="bi-eye"></i>
                        </a>
                        
                    </div>
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->

            <div class="row">

              <div class="col-sm-6">

                <div class="mb-4">
                  <!-- Select -->
                  <div class="tom-select-custom">
                      <label class="form-label" for="gender">Jenis Kelamin</label>
                      <select class="js-select form-select" name="gender" id="gender" autocomplete="off"
                              data-hs-tom-select-options='{
                                  "searchInDropdown": false,
                                  "hidePlaceholderOnSearch": true,
                                  "placeholder": "Pilih Jenis Kelamin..."
                              }' required>
                          <option value="">Jenis Kelamin</option>
                          <option value="1" {{ old('gender', isset($user) ? $user->user_personal_information->gender : '')  == 1 ? 'selected' : '' }}>Pria</option>
                          <option value="2" {{ old('gender', isset($user) ? $user->user_personal_information->gender : '')  == 2 ? 'selected' : '' }}>Wanita</option>
                      </select>

                      @if($errors->has('gender'))
                          <span style="font-style: bold; color: red;">{{ $errors->first('gender') }}</span>
                      @endif
                  </div>
                  <!-- End Select -->
                </div>
                <!-- End Form -->

              </div>
              <!-- End Col -->

              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                  <!-- Select -->
                  <div class="tom-select-custom">
                      <label class="form-label" for="religion_id">Agama</label>
                      <select class="js-select form-select" name="religion_id" id="religion_id" autocomplete="off"
                              data-hs-tom-select-options='{
                                  "searchInDropdown": false,
                                  "hidePlaceholderOnSearch": true,
                                  "placeholder": "Pilih Agama..."
                              }' required>
                          <option value="">Pilih Agama</option>
                          @forelse($religion as $id => $religion_item_name)
                              <option value="{{ $id }}" {{ old('religion_id', isset($user) ? $user->user_personal_information->religion_id : '') == $id ? 'selected' : '' }}>{{ $religion_item_name }}</option>
                          @empty
                              {{-- Not found --}}
                          @endforelse
                      </select>

                      @if($errors->has('religion_id'))
                          <span style="font-style: bold; color: red;">{{ $errors->first('religion_id') }}</span>
                      @endif
                  </div>
                  <!-- End Select -->
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->
            </div>
            <!-- End Row -->

            <div class="row">
              <div class="col-sm-6">
                <!-- Form -->
                <div class="mb-3">
                  <label class="form-label" for="birth_date">Tanggal Lahir</label>
                  <input type="text" class="form-control form-control-lg" value="{{ old('birth_date') }}" name="birth_date" id="birth_date" autocomplete="off" placeholder="Tanggal Lahir" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-inputmask-placeholder="dd/mm/yyyy" required>
                
                  @if($errors->has('birth_date'))
                      <span style="font-style: bold; color: red;">{{ $errors->first('birth_date') }}</span>
                  @endif
                </div>
                <!-- End Form -->
              </div>
              <!-- End Col -->

              <div class="col-sm-6">

                <div class="mb-4">
                  <!-- Select -->
                  <div class="tom-select-custom">
                    <label class="form-label" for="provinces_id">Provinsi</label>

                     <!-- Select -->
                    <select class="js-select form-select" name="provinces_id" id="provinces_id" autocomplete="off"
                            data-hs-tom-select-options='{
                                "searchInDropdown": false,
                                "hidePlaceholderOnSearch": true,
                                "placeholder": "Pilih Provinsi..."
                            }' required>
                        <option value="">Pilih Provinsi</option>
                        @forelse($province as $id => $province_item_name)
                            <option value="{{ $id }}" {{ old('provinces_id', isset($user) ? $user->user_personal_information->provinces_id : '') == $id ? 'selected' : '' }}>{{ $province_item_name }}</option>
                        @empty
                            {{-- Not found --}}
                        @endforelse
                    </select>
                    
                    @if($errors->has('provinces_id'))
                        <span style="font-style: bold; color: red;">{{ $errors->first('provinces_id') }}</span>
                    @endif
                  </div>
                  <!-- End Select -->
                </div>
                <!-- End Form -->

              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->

            <div class="row">
              
              <div class="col-sm-6">

                <div class="mb-4">
                  <!-- Select -->
                  <div class="tom-select-custom">
                    <label class="form-label" for="regencies_id">Kota/Kabupaten</label>

                    <!-- Select -->
                    <select class="form-select" name="regencies_id" id="regencies_id" autocomplete="off" required>
                        <option value="">Pilih Kota/Kabupaten...</option>
                        @if (isset($user) ? $user->user_personal_information->regencies_id : ''))
                            <option value="{{ old('regencies_id', isset($user) ? $user->user_personal_information->regencies_id : '') }}" selected>{{ $user->user_personal_information->regencies->name }}</option>
                        @endif
                    </select>
                    <!-- End Select -->

                    @if($errors->has('regencies_id'))
                        <span style="font-style: bold; color: red;">{{ $errors->first('regencies_id') }}</span>
                    @endif
                  </div>
                  <!-- End Select -->
                </div>
                <!-- End Form -->

              </div>
              <!-- End Col -->

              <div class="col-sm-6">

                <div class="mb-4">
                  <!-- Select -->
                  <div class="tom-select-custom">
                    <label class="form-label" for="districts_id">Kecamatan</label>

                   <!-- Select -->
                    <select class="form-select" name="districts_id" id="districts_id" autocomplete="off" required>
                        <option value="">Pilih Kecamatan...</option>
                        @if (isset($user) ? $user->user_personal_information->districts_id : ''))
                            <option value="{{ old('districts_id', isset($user) ? $user->user_personal_information->districts_id : '') }}" selected>{{ $user->user_personal_information->districts->name }}</option>
                        @endif
                    </select>
                    <!-- End Select -->

                    @if($errors->has('districts_id'))
                        <span style="font-style: bold; color: red;">{{ $errors->first('districts_id') }}</span>
                    @endif
                  </div>
                  <!-- End Select -->
                </div>
                <!-- End Form -->

              </div>
              <!-- End Col -->

            </div>
            <!-- End Row -->

            <div class="row">
              <div class="mb-3">
                <label class="form-label" for="address">Alamat</label>
                <textarea id="address" class="form-control" style="resize: none" name="address" placeholder="Masukkan Alamat" rows="8" autocomplete="off" required>{{ old('address') }}</textarea>
                
                @if($errors->has('address'))
                    <span style="font-style: bold; color: red;">{{ $errors->first('address') }}</span>
                @endif
              </div>
            </div>
          </div>

          <div class="mt-4">
            <div class="d-grid">
              <button type="submit" class="btn btn-gold btn-lg">Daftar</button>
            </div>
  
            <div class="text-center mt-3">
              <p>Sudah punya akun? <a class="link" href="{{ route('login') }}">Login disini</a></p>
            </div>
          </div>

        </form>
      </div>
    </div>
    <!-- End Form -->
  </main>
<!-- ========== END MAIN CONTENT ========== -->
@endsection

@push('after-style')
    <link rel="stylesheet" href="{{ asset('/back-design/third-party/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endpush


@push('after-script')
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/back-design/third-party/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
          $('#birth_date').datetimepicker({
              format: 'DD/MM/YYYY'
          });
    </script> 

    <script>
       // auto populate dropdown - regencies
        $(document).ready(function(){
            // provinces Change
            $('#provinces_id').change(function(){
                // provinces id
                var id = $(this).val();
                // Empty the dropdown
                $('#regencies_id').find('option').not(':first').remove();
                $('#districts_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '{{ URL::to("/api/regencies") }}'+'/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null)
                        {
                            len = response['data'].length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++)
                            {
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#regencies_id").append(option);
                            }
                        }
                    }
                });
            });
        });

        // auto populate dropdown - districts
        $(document).ready(function(){
            // regencies Change
            $('#regencies_id').change(function(){
                // regencies id
                var id = $(this).val();
                // Empty the dropdown
                $('#districts_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '{{ URL::to("/api/districts") }}'+'/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response['data'] != null)
                        {
                            len = response['data'].length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++)
                            {
                                var id = response['data'][i].id;
                                var name = response['data'][i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#districts_id").append(option);
                            }
                        }
                    }
                });
            });
        });

    </script>

@endpush
