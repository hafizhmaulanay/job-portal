@extends('layouts.auth')

@section('title', 'Lupa Password')

@section('content')
    <main id="content" role="main">
        <!-- Form -->
        <div class="container content-space-3 content-space-t-lg-3 content-space-b-lg-3">
            <div class="flex-grow-1 mx-auto" style="max-width: 28rem;">
                <!-- Heading -->
                <div class="text-center mb-5 mb-md-7">
                    <a href="{{ route('home') }}">
                        <img class="w-100 mb-2" style="max-width: 32rem" src="{{ asset('front-design/client-assets/images/penara-text-image.png') }}" alt="Logo Penara">
                    </a>
                    <h1 class="h2">Lupa Password?</h1>
                    <p>
                        Masukkan email Anda untuk reset password.
                    </p>
                </div>
                <!-- End Heading -->

                @if (session('status'))
                    <div class="alert alert-soft-success text-center" style="margin-top: -20px; margin-bottom: 20px" role="alert">
                        <div class="fw-semi-bold">
                           Kami telah mengirim email link reset kata sandi Anda!
                        </div>
                    </div>
                @endif

                <!-- Error -->
                @if ($errors->any())
                    <div class="alert alert-soft-danger alert-dismissible fade show" style="margin-top: -20px; margin-bottom: 20px" role="alert">
                        <div class="fw-semi-bold">{{ __('Ups! Ada yang salah.') }}</div>
                        <ul class="mt-3 mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <!-- End Error -->

                <!-- Form -->
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="mb-3">
                        <div class="d-flex justify-content-between align-items-center">
                        <label class="form-label" for="email" tabindex="0">Email</label>

                        <a class="form-label-link" href="{{ route('login') }}">
                            <i class="bi-chevron-left small ms-1"></i> Back to Log in
                        </a>
                        </div>

                        <input type="email" class="form-control form-control-lg" name="email" id="email" value="{{ old('email') }}" tabindex="1" placeholder="Masukkan email anda" aria-label="Masukkan email anda" required="" autofocus>
                    </div>

                    <div class="d-grid mb-3">
                        <button type="submit" class="btn btn-gold btn-lg fs-4">
                            Submit
                        </button>
                    </div>
                </form>
                <!-- End Form -->
            </div>
        </div>
        <!-- End Form -->
    </main>
@endsection
