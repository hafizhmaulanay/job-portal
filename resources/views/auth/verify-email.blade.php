@extends('layouts.auth')

@section('title', 'Verify Email')

@section('content')
    <main id="content" role="main">
        <!-- Form -->
        <div class="container content-space-3 content-space-t-lg-3 content-space-b-lg-3">
            <div class="flex-grow-1 mx-auto" style="max-width: 28rem;">
                <!-- Heading -->
                <div class="text-center mb-5 mb-md-7">
                    <a href="{{ route('home') }}">
                        <img class="w-100 mb-4" style="max-width: 32rem" src="{{ asset('front-design/client-assets/images/penara-text-image.png') }}" alt="Logo Penara">
                    </a>
                    <h1 class="h2">Silahkan verifikasi email Anda!</h1>
                    <p>Periksa email Anda untuk melakukan verifikasi email.</p>
                </div>
                <!-- End Heading -->

                 @if (session('status') == 'verification-link-sent')
                    <div class="alert alert-soft-success text-center" style="margin-top: -20px; margin-bottom: 20px" role="alert">
                        <div class="fw-semi-bold">
                            {{ __('Email verifikasi telah dikirim ke alamat email Anda.') }}
                        </div>
                    </div>
                 @endif

                <!-- Form -->
                 <form method="POST" action="{{ route('verification.send') }}">
                    @csrf

                    <div class="d-grid mb-3">
                        <button type="submit" class="btn btn-gold btn-lg fs-4">
                             <i class="bi bi-envelope me-1"></i> Kirim Ulang Email
                        </button>
                    </div>
                </form>
                <!-- End Form -->

                <!-- logout -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <div class="d-grid mb-3">
                        <button type="submit" class="btn btn-outline-gold btn-lg">
                            {{ __('Log Out') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Form -->
    </main>
@endsection
