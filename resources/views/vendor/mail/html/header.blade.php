<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'PENARA Jobs Portal')
<img src="{{ asset('/back-design/clients/img/logo.png') }}" class="logo" alt="Penara Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
