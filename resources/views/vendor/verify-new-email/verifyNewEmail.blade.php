@component('mail::message')
# Verifikasi Email Address Baru

Silakan klik tombol di bawah ini untuk memverifikasi alamat email Anda.

@component('mail::button', ['url' => $url])
Verifikasi Email Address Baru
@endcomponent

Jika Anda tidak memperbarui alamat email Anda, tidak ada tindakan lebih lanjut yang diperlukan.

Dengan Hormat,<br>
{{ 'PT. PENA BERSINAR NUSANTARA' }}
@endcomponent