<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="{{ url('https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css') }}">
<link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css') }}">
<link rel="stylesheet" href="{{ asset('front-design/assets/vendor/hs-mega-menu/dist/hs-mega-menu.min.css') }}">
<link rel="stylesheet" href="{{ asset('front-design/assets/vendor/nouislider/dist/nouislider.min.css') }}">
<link rel="stylesheet" href="{{ asset('front-design/assets/vendor/tom-select/dist/css/tom-select.bootstrap5.css') }}">

<!-- CSS Front Template -->
<link rel="stylesheet" href="{{ asset('front-design/assets/css/theme.min.css') }}">