<!-- JS Global Compulsory  -->
<script src="{{ asset('front-design/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<!-- JS Implementing Plugins -->
<script src="{{ asset('front-design/assets/vendor/hs-mega-menu/dist/hs-mega-menu.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/hs-show-animation/dist/hs-show-animation.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/hs-go-to/dist/hs-go-to.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/tom-select/dist/js/tom-select.complete.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/hs-toggle-password/dist/js/hs-toggle-password.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/imask/dist/imask.min.js') }}"></script>

<!-- JS Front -->
<script src="{{ asset('front-design/assets/js/theme.min.js') }}"></script>

{{-- inputmask --}}
<script src="{{ asset('front-design/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('back-design/third-party/inputmask/dist/jquery.inputmask.js') }}"></script>
<script src="{{ asset('back-design/third-party/inputmask/dist/inputmask.js') }}"></script>

<!-- JS Plugins Init. -->
<script>
    (function() {
        // INITIALIZATION OF MEGA MENU
        // =======================================================
        new HSMegaMenu('.js-mega-menu', {
            desktop: {
            position: 'left'
            }
        })


        // INITIALIZATION OF BOOTSTRAP VALIDATION
        // =======================================================
        HSBsValidation.init('.js-validate',)


        // INITIALIZATION OF SELECT
        // =======================================================
        HSCore.components.HSTomSelect.init('.js-select')


        // INITIALIZATION OF INPUT MASK
        // =======================================================
        HSCore.components.HSMask.init('.js-input-mask')


        // INITIALIZATION OF TOGGLE PASSWORD
        // =======================================================
        new HSTogglePassword('.js-toggle-password')


        // INITIALIZATION OF SHOW ANIMATIONS
        // =======================================================
        new HSShowAnimation('.js-animation-link')


        // INITIALIZATION OF BOOTSTRAP DROPDOWN
        // =======================================================
        HSBsDropdown.init()


        // INITIALIZATION OF GO TO
        // =======================================================
        new HSGoTo('.js-go-to')

    })()
</script>

<script>
    // INITIALIZATION INPUTMASK
    // =======================================================
    $(":input").inputmask();
</script>