<!-- Navbar -->
<div class="navbar-expand-lg navbar-light">
    <div id="sidebarNav" class="collapse navbar-collapse navbar-vertical">
        <!-- Card -->
        <div class="card flex-grow-1 mb-5">
            <div class="card-body">
                <!-- Avatar -->
                <div class="d-none d-lg-block text-center mb-5">
                    <div class="avatar avatar-xxl avatar-circle mb-3">
                         <img  src=" 
                            @if (auth()->user()->user_personal_information()->first()->photo_url != "" || auth()->user()->user_personal_information()->first()->photo_url != null) 
                                @if(File::exists('storage/'.substr(auth()->user()->user_personal_information()->first()->photo_url, strpos(auth()->user()->user_personal_information()->first()->photo_url, 'assets/'))))
                                    
                                    {{ auth()->user()->user_personal_information()->first()->photo_url }}
                                
                                @elseif(File::exists(str_replace(substr(auth()->user()->user_personal_information()->first()->photo_url, 0, strpos(auth()->user()->user_personal_information()->first()->photo_url, 'assets/')), 'storage/app/public/', auth()->user()->user_personal_information()->first()->photo_url)))
                                    {{ url(str_replace(substr(auth()->user()->user_personal_information()->first()->photo_url, 0, strpos(auth()->user()->user_personal_information()->first()->photo_url, 'assets/')), 'storage/app/public/', auth()->user()->user_personal_information()->first()->photo_url)) }}
                                @else
                                    {{ asset('front-design/assets/img/160x160/img1.jpg') }}
                                @endif
                            @else 
                                {{ asset('front-design/assets/img/160x160/img1.jpg') }} 
                            @endif " 
                            alt="users avatar" class="avatar-img">
                            @if (Auth::check() && Auth::user()->email_verified_at)
                                <img class="avatar-status avatar-lg-status" src="{{ asset('front-design/assets/svg/illustrations/top-vendor.svg') }}" alt="Image Description" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Verified user" aria-label="Verified user">
                            @endif
                    </div>

                    <h4 class="card-title mb-0">{{ Auth::user()->name ?? '' }}</h4>
                    <p class="card-text small">{{ Auth::user()->email ?? '' }}</p>
                </div>
                <!-- End Avatar -->

                <!-- Nav -->
                <span class="text-cap">Akun</span>

                <!-- List -->
                <ul class="nav nav-sm nav-tabs nav-vertical mb-4">
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('profile') || request()->is('profile/*') ? ' active' : '' }}" href="{{ route('profile.index') }}">
                            <i class="bi-person nav-icon"></i> Profile
                        </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link{{ request()->is('profile/security') || request()->is('security') || request()->is('security/*') ? ' active' : '' }}" href="{{ route('security.profile') }}">
                        <i class="bi-shield-shaded nav-icon"></i> keamanan
                      </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('profile/work_experience') || request()->is('work_experience') || request()->is('work_experience/*') ? ' active' : '' }}" href="{{ route('work_experience.profile') }}">
                            <i class="bi-binoculars nav-icon"></i> Pengalaman Kerja
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('profile/education') || request()->is('education') || request()->is('education/*') ? ' active' : '' }}" href="{{ route('education.profile') }}">
                            <i class="bi-mortarboard nav-icon"></i> Pendidikan
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('profile/skill') || request()->is('skill') || request()->is('skill/*') ? ' active' : '' }}" href="{{ route('skill.profile') }}">
                            <i class="bi-tools nav-icon"></i> Keahlian
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('profile/resume') || request()->is('resume') || request()->is('resume/*') ? ' active' : '' }}" href="{{ route('resume.profile') }}">
                            <i class="bi-file-earmark-person nav-icon"></i> Resume
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('profile/job_wishlist') || request()->is('job_wishlist') || request()->is('job_wishlist/*') ? ' active' : '' }}" href="{{ route('job_wishlist.profile') }}">
                            <i class="bi bi-save nav-icon"></i> Lowongan tersimpan
                        </a>
                    </li>
                </ul>
                <!-- End List -->

                {{-- <span class="text-cap">Lamaran</span>

                <!-- List -->
                <ul class="nav nav-sm nav-tabs nav-vertical mb-4">
                    <li class="nav-item">
                        <a class="nav-link{{ request()->is('job_vacancy') || request()->is('job_vacancy/*') ? ' active' : '' }}" href="{{ route('job_vacancy.index') }}">
                            <i class="bi-briefcase nav-icon"></i> Semua lamaran
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="./account-wishlist.html">
                            <i class="bi-clock nav-icon"></i> Dalam review
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="./account-wishlist.html">
                            <i class="bi-pen nav-icon"></i> Jadwal test
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="./account-wishlist.html">
                            <i class="bi-chat-left-dots nav-icon"></i> Jadwal interview
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="./account-wishlist.html">
                            <i class="bi-check-lg nav-icon"></i> Penawaran
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="./account-wishlist.html">
                            <i class="bi-x-lg nav-icon"></i> Tidak lolos
                        </a>
                    </li>
                </ul>
                <!-- End List --> --}}
            </div>
        </div>
        <!-- End Card -->
    </div>
</div>
<!-- End Navbar -->