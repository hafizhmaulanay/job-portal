<!-- JS Global Compulsory  -->
<script src="{{ asset('front-design/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<!-- JS Implementing Plugins -->
<script src="{{ asset('front-design/assets/vendor/hs-mega-menu/dist/hs-mega-menu.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/hs-show-animation/dist/hs-show-animation.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/hs-go-to/dist/hs-go-to.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/nouislider/dist/nouislider.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/fslightbox/index.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/tom-select/dist/js/tom-select.complete.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/hs-file-attach/dist/hs-file-attach.min.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/hs-toggle-password/dist/js/hs-toggle-password.js') }}"></script>
<script src="{{ asset('front-design/assets/vendor/imask/dist/imask.min.js') }}"></script>

<!-- JS Front -->
<script src="{{ asset('front-design/assets/js/theme.min.js') }}"></script>

{{-- inputmask --}}
<script src="{{ asset('front-design/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('back-design/third-party/inputmask/dist/jquery.inputmask.js') }}"></script>
<script src="{{ asset('back-design/third-party/inputmask/dist/inputmask.js') }}"></script>

{{-- toastify  --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>

<!-- JS Plugins Init. -->
<script>
    (function() {
        // INITIALIZATION OF MEGA MENU
        // =======================================================
        new HSMegaMenu('.js-mega-menu', {
            desktop: {
            position: 'left'
            }
        })


        // INITIALIZATION OF SHOW ANIMATIONS
        // =======================================================
        new HSShowAnimation('.js-animation-link')


        // INITIALIZATION OF BOOTSTRAP DROPDOWN
        // =======================================================
        HSBsDropdown.init()


        // INITIALIZATION OF INPUT MASK
        // =======================================================
        HSCore.components.HSMask.init('.js-input-mask')


        // INITIALIZATION OF TOGGLE PASSWORD
        // =======================================================
        new HSTogglePassword('.js-toggle-password')


        // INITIALIZATION OF FILE ATTACH
        // =======================================================
        new HSFileAttach('.js-file-attach')


        // INITIALIZATION OF SELECT
        // =======================================================
        var tomSelect = HSCore.components.HSTomSelect.init('.js-select')
        $('.js-select-clear').on('click', function (e) {
            e.preventDefault()
            tomSelect.clear()
        })


        // INITIALIZATION OF GO TO
        // =======================================================
        new HSGoTo('.js-go-to')


        // INITIALIZATION OF SWIPER
        // =======================================================
        var swiper = new Swiper('.js-swiper-employer-jobs',{
            slidesPerView: 1,
            navigation: {
            nextEl: '.js-swiper-employer-jobs-button-next',
            prevEl: '.js-swiper-employer-jobs-button-prev',
            },
            breakpoints: {
            480: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 15,
            },
            }
        });


        // INITIALIZATION OF NOUISLIDER`
        // =======================================================
        HSCore.components.HSNoUISlider.init('.js-nouislider')
    })()
</script>

<script>
    // INITIALIZATION INPUTMASK
    // =======================================================
    $(":input").inputmask();
</script>