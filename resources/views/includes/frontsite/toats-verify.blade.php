@if (Auth::check() && !Auth::user()->email_verified_at)
    <!-- Toast -->
    <div id="liveToast" class="d-none d-lg-block position-fixed toast text-white bg-danger show" role="alert" aria-live="assertive" aria-atomic="true" style="width: 500px; bottom: 20px; left: 30px; z-index: 1000;">
        <div class="toast-body d-flex align-items-center justify-content-between">
            <div>
                <div>
                    Your email : {{ Auth::user()->email }}
                </div>
                <div class="fs-5 mt-1">
                    Silahkan verifikasi email Anda.
                </div>
            </div>
            <div class="ml-auto">
                <form method="POST" action="{{ route('verification.send') }}">
                    @csrf
                    <button type="submit" class="btn btn-sm btn-outline-light" data-dismiss="toast" aria-label="Close">
                        <i class="bi bi-envelope me-1"></i> Kirim Ulang
                    </button>
                </form>
            </div>
        </div>
    </div>
    <!-- End Toast -->

    <!-- Toast -->
    <div id="liveToast" class="d-block d-lg-none position-fixed toast text-white bg-danger show start-50" role="alert" aria-live="assertive" aria-atomic="true" style="bottom: 0; transform: translate(-50%, -20%) !important; z-index: 1000;">
        <div class="toast-body d-flex align-items-center justify-content-between">
            <div>
                <div>
                    Your email : {{ Auth::user()->email }}
                </div>
                <div class="fs-5 mt-1">
                    Silahkan verifikasi email Anda.
                </div>
            </div>
            <div class="ml-auto">
                <form method="POST" action="{{ route('verification.send') }}">
                    @csrf
                    <button type="submit" class="btn btn-sm btn-outline-light" data-dismiss="toast" aria-label="Close">
                        <i class="bi bi-envelope"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <!-- End Toast -->
@endif

@push('after-script')    
    <script>
        @if (session('status') == 'verification-link-sent')
            Toastify({
                // icon check
                text: "Link verifikasi telah dikirim ke email Anda.",
                style: {
                background: "#00C9A7",
                color: "#ffffff",
                fontSize: "15px",
                textAlign: "center"
                },
                posisition: "top-center",
            }).showToast();
        @endif
    </script>
@endpush