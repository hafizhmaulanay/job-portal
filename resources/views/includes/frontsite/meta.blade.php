<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="The Pen4ra Jobs &amp; modern responsive website internal for management security.">
<meta name="keywords" content="pen4ra, jobs portal, jobs, management office, portal jobs">
<meta name="author" content="Artbycode">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
