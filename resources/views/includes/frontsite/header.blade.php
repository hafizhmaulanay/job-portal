<!-- ========== HEADER ========== -->
<header id="header" class="navbar navbar-expand-lg navbar-end navbar-light">

    <div class="container py-2">
      <nav class="js-mega-menu navbar-nav-wrap">
        <!-- Default Logo -->
        <a class="pe-3" href="{{ route('home') }}" aria-label="Front">
          <img class="" style="max-width: 100px; max-height: 90px" src="{{ asset('back-design/clients/img/logo.png') }}" alt="Logo">
        </a>
        <!-- End Default Logo -->

        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-default">
            <i class="bi-list"></i>
          </span>
          <span class="navbar-toggler-toggled">
            <i class="bi-x"></i>
          </span>
        </button>
        <!-- End Toggler -->
      
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav justify-content-start">

            @guest
              <li class="nav-item">
                <a class="nav-link{{ request()->is('company') ? ' active' : '' }}" href="{{ route('company.index') }}">Perusahaan</a>
              </li>
            @endguest
          
            <li class="nav-item">
              <a class="nav-link{{ request()->is('announcement*') ? ' active' : '' }}" href="{{ route('announcement.index') }}">Pengumuman</a>
            </li>
          </ul>

          <ul class="navbar-nav justify-content-end mt-lg-1">
            @auth
              <li class="hs-has-sub-menu nav-item">
                <a id="companyMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  {{ Auth::user()->name ?? '' }}
                </a>

                <!-- Mega Menu -->
                <div class="hs-sub-menu dropdown-menu" aria-labelledby="companyMegaMenu" style="min-width: 14rem;">
                  <a class="dropdown-item" href="{{ route('profile.index') }}">Profile</a>
                  <a class="dropdown-item" href="{{ route('applicant.my_application') }}">Lamaran Saya</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item text-danger" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                      @csrf
                  </form>
                </div>
                <!-- End Mega Menu -->
              </li>
            @else
              <li class="nav-item">
                <a class="btn btn-outline-gold d-grid d-lg-block mb-3 mb-lg-0" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="btn btn-gold btn-transition d-grid d-lg-block" href="{{ route('register') }}">Register</a>
              </li>
            @endauth
          </ul>
        </div>
        <!-- End Collapse -->
      </nav>
    </div>
</header>
<!-- ========== END HEADER ========== -->