<!-- ========== FOOTER ========== -->
<footer class="bg-gold">
    <div class="container py-4 py-lg-6 text-center">
       <!-- Logo -->
      <a class="d-inline-flex align-items-center mb-3" href="{{ route('home') }}" aria-label="Front">
        <img class="w-100" style="max-width: 32rem" src="{{ asset('front-design/client-assets/images/penara-text-image.png') }}" alt="Logo">
      </a>
      <!-- End Logo -->

      <p class="text-white-50 fs-5 mb-1">Ruko Jewalk Jl. DR. Wahidin Sudirohusodo no 3 (Depan Kolam Renang Golf Jababeka)</p>
      <p class="text-white-50 fs-5 mb-5">Jatireja, Jababeka, Cikarang Timur, Kab. Bekasi.</p>

      <div class="border-top border-white-10 my-7"></div>

      <!-- Copyright -->
      <div class="w-100 text-lg-center mx-lg-auto">
        <p class="text-white-50 small">© Penara Job {{ date('Y') }}. All rights reserved.</p>
      </div>
      <!-- End Copyright -->
</footer>
<!-- ========== END FOOTER ========== -->